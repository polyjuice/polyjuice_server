# Polyjuice Server

Polyjuice Server is a [Matrix](https://matrix.org/) server library.  The
goal is to help build a Matrix homeserver by providing a framework for
implementing the server functionality.

It uses `Plug` for serving endpoints.  The general idea is that the plug takes
a `server:` parameter, which is something that implements the logic necessary
for fulfilling the request.  The server would not have to implement everything,
but only the portions that it wants to implement.  For example, there is a room
directory protocol, and servers that want to provide room directory
functionality would implement that protocol, but servers that do not want to
provide a room directory would not, and Polyjuice Server will respond
appropriately to requests.

It also contains functions for making federation calls.  The interface for this
is similar to [Polyjuice Client](https://hex.pm/packages/polyjuice_client/).

## Installation

**Warning:** this library is unstable, and the API may change at any point.

The package can be installed by adding `polyjuice_server` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:polyjuice_server, git: "https://gitlab.com/uhoreg/polyjuice_server.git", ref: "<the_commit_that_you_are_using>"}
  ]
end
```

where the appropriate substitution is made.  I highly recommend pinning to a
specific commit rather than a branch to ensure that changes in the library do
not unexpectedly break your code.  Alternatively, just wait until there is a
stable release.

## Writing a Server

To write a server, you will first need to create a server module that
implements the protocols (from `Polyjuice.Server.Protocols.*`) for the parts of
the server that you want implemented.  If you want to handle client-server
connections, you will need to implement
`Polyjuice.Server.Protocols.BaseClientServer`, and if you want to handle
federation, you will need to implement
`Polyjuice.Server.Protocols.BaseFederation`.  For federation, you should also
implement the `Polyjuice.Server.Federation.Client.API` protocol so that it can
make federation requests.  The easiest way to do that is to create a
`Polyjuice.Server.Federation.Client` struct and pass all
`Polyjuice.Server.Federation.Client.API` calls to that.

Next, you will need to serve the appropriate `Plug` using a plug adapter.  In
general, serving `Polyjuice.Server.Plug.Matrix` will work.  The plugs expect
the server struct to be passed as an option under the name `server:`.  For
example, if you use `Plug.Cowboy`, you could call:

```elixir
Plug.Cowboy.http Polyjuice.Server.Plug.Matrix [server: server]
```

### Protocol helper

TODO: ...

## Other Polyjuice libraries

- [Polyjuice Util](https://hex.pm/packages/polyjuice_util/) general Matrix functions
- [Polyjuice Client](https://hex.pm/packages/polyjuice_client/) Matrix client library

<!--
SPDX-FileCopyrightText: 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
SPDX-License-Identifier: CC0-1.0
-->
