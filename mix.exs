defmodule PolyjuiceServer.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_server,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),

      # Docs
      name: "Polyjuice Server",
      source_url: "https://gitlab.com/polyjuice/polyjuice_server",
      homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/polyjuice/polyjuice_server"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: 0]
      ],
      consolidate_protocols: Mix.env() != :test
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.12"},
      {:jason, "~> 1.2"},
      {:plug, "~> 1.11"},
      {:polyjuice_util, "~> 0.2.2"},
      {:plug_cowboy, "~> 2.0", only: :dev}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
