# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.PubSub do
  alias Polyjuice.Server.Protocols.Helper

  @callback publish(args :: {any, list()}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(any)

  @callback subscribe(
              args :: {Polyjuice.Server.Protocols.Subscriber.t(), list()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(any)

  @callback unsubscribe(
              args :: {Polyjuice.Server.Protocols.Subscriber.t(), list()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(any)

  @callback await_sync(
              args ::
                {String.t(), String.t(), Polyjuice.Server.SyncToken.t() | nil, non_neg_integer,
                 map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {Polyjuice.Server.SyncToken.t() | nil, map()}
                | :logout
                | :soft_logout
              )

  @callback get_sync_data_since(
              args :: {String.t(), String.t(), Polyjuice.Server.SyncToken.t() | nil, map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {Polyjuice.Server.SyncToken.t() | nil, map()}
                | :logout
                | :soft_logout
              )

  @callback add_filter(
              args :: {String.t(), String.t(), map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(String.t())

  @callback get_filter(
              args :: {String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result({:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()})
end
