# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.Helper.PushRule do
  @callback get_push_rules(
              args :: {String.t(), String.t() | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{optional(String.t()) => %{optional(String.t()) => list(map())}})

  @callback get_push_rule(
              args :: {String.t(), String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result({:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()})
end
