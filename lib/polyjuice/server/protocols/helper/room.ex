# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.Room do
  alias Polyjuice.Server.Protocols.Helper

  @callback create(
              args ::
                {list({String.t(), String.t(), String.t(), Polyjuice.Util.event_content()})},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, String.t()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback join(
              args :: {String.t(), String.t(), Keyword.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, :unknown_room}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback send_event(
              args ::
                {String.t(), String.t(), String.t(), String.t(), String.t() | nil,
                 String.t() | nil, Polyjuice.Util.event_content()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, String.t()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_state(
              args :: {String.t(), String.t(), String.t() | nil, String.t() | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, list(Polyjuice.Util.event())}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_messages(
              args :: {String.t(), String.t(), String.t(), :forward | :backward, Keyword.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, list(Polyjuice.Util.event()), String.t() | nil,
                 list(Polyjuice.Util.event())}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_event(
              args :: {String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, Polyjuice.Util.event()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback forget(args :: {String.t(), String.t()}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback kick(
              args :: {String.t(), String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback invite(
              args :: {String.t(), String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback unban(
              args :: {String.t(), String.t(), String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )
end
