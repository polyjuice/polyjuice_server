# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.User do
  alias Polyjuice.Server.Protocols.Helper

  @callback login_flows(
              args :: {},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(list(map) | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback log_in(
              args :: {{:password, String.t(), String.t()} | {:token, String.t()}, Keyword.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result({:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback log_out(
              args :: {String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) :: Helper.call_result(any)

  @callback get_user_devices(
              args :: {String.t()},
              from :: GenServer.from(),
              state :: term
            ) :: Helper.call_result(list(String.t()))

  @callback register(
              args :: {:user | :guest, String.t(), String.t(), boolean, Keyword.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, map()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback change_password(
              args :: {String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) :: Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback deactivate(
              args :: {String.t()},
              from :: GenServer.from(),
              state :: term
            ) :: Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback user_interactive_auth(
              args :: {String.t(), atom, String.t(), map() | nil, map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, map()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_device_info(
              args :: {String.t(), String.t(), String.t() | :all},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, %{optional(String.t()) => map()}}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback set_device_info(
              args :: {String.t(), String.t(), %{optional(String.t()) => any}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_profile(
              args :: {String.t(), String.t(), String.t() | :all},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, %{optional(String.t()) => map()}}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback set_profile(
              args :: {String.t(), %{optional(String.t()) => any}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                :ok
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )
end
