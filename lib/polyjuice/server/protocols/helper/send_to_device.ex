# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.SendToDevice do
  @callback send_to_device(
              args :: {String.t(), String.t(), String.t(), String.t(), map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback mark_received(
              args :: {String.t(), String.t(), non_neg_integer()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok)
end
