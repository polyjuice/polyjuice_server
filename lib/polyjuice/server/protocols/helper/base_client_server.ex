# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.BaseClientServer do
  alias Polyjuice.Server.Protocols.Helper

  @callback get_server_name(args :: {}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(String.t())

  @callback user_from_access_token(
              args :: {String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:user, String.t(), String.t(), boolean}
                | {:application_service, String.t()}
                | nil
                | {:soft_logout, String.t(), String.t(), boolean}
              )

  @callback user_in_appservice_namespace(
              args :: {String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(boolean | :exclusive)
end
