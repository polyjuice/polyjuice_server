# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.DeviceKey do
  @callback set_device_keys(
              args :: {String.t(), String.t(), map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback add_one_time_keys(
              args :: {String.t(), String.t(), %{String.t() => map()}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback set_cross_signing_keys(
              args :: {String.t(), %{String.t() => map()}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback add_signatures(
              args :: {String.t(), %{String.t() => %{String.t() => map()}}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{optional(String.t()) => %{String.t() => Jason.Encoder}})

  @callback claim_key(
              args :: {%{String.t() => %{String.t() => String.t()}}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{
                optional(String.t()) => %{String.t() => %{String.t() => any}}
              })

  @callback query_keys(
              args ::
                {String.t() | nil, %{String.t() => list(String.t())}, non_neg_integer | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{
                String.t() => %{optional(String.t()) => %{String.t() => map}}
              })

  @callback get_changes(
              args :: {String.t(), non_neg_integer, non_neg_integer | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result({list(String.t()), list(String.t())})

  @callback key_counts(
              args :: {String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{
                one_time_keys: %{optional(String.t()) => non_neg_integer},
                unused_fallback_keys: list(String.t())
              })
end
