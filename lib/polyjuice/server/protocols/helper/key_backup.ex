# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.KeyBackup do
  alias Polyjuice.Server.Protocols.Helper

  @callback put_keys(
              args ::
                {String.t(), String.t(),
                 %{
                   optional(String.t()) => %{
                     String.t() => Polyjuice.Server.Protocols.KeyBackup.backup_data()
                   }
                 }},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, non_neg_integer, String.t()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback get_keys(
              args ::
                {String.t(), String.t(), String.t() | nil, String.t() | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok,
                 %{
                   optional(String.t()) => %{
                     String.t() => Polyjuice.Server.Protocols.KeyBackup.backup_data()
                   }
                 }}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback delete_keys(
              args ::
                {String.t(), String.t(), String.t() | nil, String.t() | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, non_neg_integer, String.t()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback create_version(
              args ::
                {String.t(), String.t(), map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok, String.t()}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback update_version(
              args ::
                {String.t(), String.t(), String.t(), map()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})

  @callback get_version(
              args ::
                {String.t(), String.t() | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(
                {:ok,
                 %{
                   algorithm: String.t(),
                   auth_data: map(),
                   count: non_neg_integer,
                   etag: String.t(),
                   version: String.t()
                 }}
                | {:error, Polyjuice.Server.Plug.MatrixError.t()}
              )

  @callback delete_version(
              args ::
                {String.t(), String.t()},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok | {:error, Polyjuice.Server.Plug.MatrixError.t()})
end
