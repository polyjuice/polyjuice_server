# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper do
  @moduledoc """
  Helper for writing server implementations.

  Implements protocols by sending a `GenServer` call to the server process, and
  then delegating the handling of the call to a module.

  If you do not want to use a `GenServer`, but want to do something similar,
  you can use the `handle_protocol` macro.

  """

  @typedoc """
  The result of a callback.  This is the return type from `GenServer.call/3`,
  parameterized by the expected reply type.
  """
  @type call_result(reply) ::
          {:reply, reply, term}
          | {:reply, reply, term, timeout | :hibernate | {:continue, term}}
          | {:noreply, term}
          | {:noreply, term, timeout | :hibernate | {:continue, term}}
          | {:stop, term, reply, term}
          | {:stop, term, term}

  @doc false
  defmacro __using__(opts) do
    delegates = Keyword.get(opts, :delegates, [])

    quote do
      use GenServer
      import Polyjuice.Server.Protocols.Helper, only: [handle_protocol: 2]

      @doc false
      @impl GenServer
      def handle_call({protocol, func, args} = msg, from, state) do
        handler = Enum.find(unquote(delegates), fn {p, _} -> p == protocol end)

        case handler do
          {_, module} -> apply(module, func, [args, from, state])
          _ -> __MODULE__.handle_call_other(msg, from, state)
        end
      end

      def handle_call(msg, from, state) do
        __MODULE__.handle_call_other(msg, from, state)
      end

      def handle_call_other(msg, _from, state), do: {:stop, {:bad_call, msg}, state}

      defoverridable handle_call_other: 3

      Enum.each(
        unquote(delegates),
        fn {protocol, _} ->
          handle_protocol(protocol, fn
            %{pid: pid},
            Polyjuice.Server.Protocols.PubSub = protocol,
            :await_sync = func,
            {_, _, _, timeout, _} = args ->
              GenServer.call(pid, {protocol, func, args}, timeout + 10_000)

            %{pid: pid}, protocol, func, args ->
              GenServer.call(pid, {protocol, func, args})
          end)
        end
      )
    end
  end

  @doc """
  Handle a protocol using a function.

  `call` is a function that takes four arguments:
  - the server object that the function was called with
  - the protocol to be handled
  - the function name to be handled
  - the arguments to the function, as a tuple.

  For example, when
  `Polyjuice.Server.Protocols.BaseClientServer.user_from_access_token(server,
  access_token)` is called, then `call(server, Protocols.BaseClientServer,
  :user_from_access_token, {access_token})` will be called.

  """
  defmacro handle_protocol(protocol, call) do
    quote bind_quoted: [protocol: protocol], unquote: true do
      # putting `call` in `bind_quoted` doesn't work for some reason (nor does
      # `call = unquote(call)`), so we have to `unquote` everywhere
      case protocol do
        Polyjuice.Server.Protocols.BaseClientServer ->
          defimpl Polyjuice.Server.Protocols.BaseClientServer do
            def get_server_name(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseClientServer,
                :get_server_name,
                {}
              )
            end

            def user_from_access_token(
                  server,
                  access_token
                )
                when is_binary(access_token) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseClientServer,
                :user_from_access_token,
                {access_token}
              )
            end

            def user_in_appservice_namespace?(
                  server,
                  appservice_user_id,
                  user_id
                )
                when is_binary(appservice_user_id) and is_binary(user_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseClientServer,
                :user_in_appservice_namespace?,
                {appservice_user_id, user_id}
              )
            end
          end

        Polyjuice.Server.Protocols.BaseFederation ->
          defimpl Polyjuice.Server.Protocols.BaseFederation do
            def get_server_name(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseFederation,
                :get_server_name,
                {}
              )
            end

            def get_keys(server) do
              unquote(call).(server, Polyjuice.Server.Protocols.BaseFederation, :get_keys, [])
            end

            def get_old_keys(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseFederation,
                :get_old_keys,
                {}
              )
            end

            def get_cached_remote_keys(
                  server,
                  origin,
                  key_ids
                )
                when is_binary(origin) and (is_list(key_ids) or key_ids == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseFederation,
                :get_cached_remote_keys,
                {origin, key_ids}
              )
            end

            def cache_remote_keys(
                  server,
                  origin,
                  keys
                )
                when is_binary(origin) and is_map(keys) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.BaseFederation,
                :cache_remote_keys,
                {origin, keys}
              )
            end
          end

        Polyjuice.Server.Protocols.AccountData ->
          defimpl Polyjuice.Server.Protocols.AccountData do
            def set(server, user_id, room_id, type, data)
                when is_binary(user_id) and (is_binary(room_id) or room_id == nil) and
                       is_binary(type) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.AccountData,
                :set,
                {user_id, room_id, type, data}
              )
            end

            def get(server, user_id, room_id, type)
                when is_binary(user_id) and (is_binary(room_id) or room_id == nil) and
                       is_binary(type) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.AccountData,
                :get,
                {user_id, room_id, type}
              )
            end
          end

        Polyjuice.Server.Protocols.DeviceKey ->
          defimpl Polyjuice.Server.Protocols.DeviceKey do
            def set_device_keys(server, user_id, device_id, device_keys)
                when is_binary(user_id) and is_map(device_keys) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :set_device_keys,
                {user_id, device_id, device_keys}
              )
            end

            def add_one_time_keys(server, user_id, device_id, one_time_keys)
                when is_binary(user_id) and is_binary(device_id) and is_map(one_time_keys) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :add_one_time_keys,
                {user_id, device_id, one_time_keys}
              )
            end

            def set_cross_signing_keys(server, user_id, device_keys)
                when is_binary(user_id) and is_map(device_keys) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :set_cross_signing_keys,
                {user_id, device_keys}
              )
            end

            def add_signatures(server, user_id, signatures)
                when is_binary(user_id) and is_map(signatures) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :add_signatures,
                {user_id, signatures}
              )
            end

            def claim_key(server, one_time_keys) when is_map(one_time_keys) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :claim_key,
                {one_time_keys}
              )
            end

            def query_keys(server, user_id, device_keys, token)
                when (is_binary(user_id) or user_id == nil) and is_map(device_keys) and
                       (is_integer(token) or token == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :query_keys,
                {user_id, device_keys, token}
              )
            end

            def get_changes(server, user_id, %Polyjuice.Server.SyncToken{} = from, to)
                when is_binary(user_id) and (is_map(to) or to == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :get_changes,
                {user_id, from, to}
              )
            end

            def key_counts(server, user_id, device_id)
                when is_binary(user_id) and is_binary(device_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.DeviceKey,
                :key_counts,
                {user_id, device_id}
              )
            end
          end

        Polyjuice.Server.Protocols.Discovery ->
          defimpl Polyjuice.Server.Protocols.Discovery do
            def address(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Discovery,
                :address,
                {}
              )
            end

            def identity_server(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Discovery,
                :identity_server,
                {}
              )
            end

            def extra_fields(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Discovery,
                :extra_fields,
                {}
              )
            end

            def delegated_hostname(server) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Discovery,
                :delegated_hostname,
                {}
              )
            end
          end

        Polyjuice.Server.Protocols.KeyBackup ->
          defimpl Polyjuice.Server.Protocols.KeyBackup do
            def put_keys(server, user_id, version, data)
                when is_binary(user_id) and is_binary(version) and is_map(data) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :put_keys,
                {user_id, version, data}
              )
            end

            def get_keys(server, user_id, version, room_id, session_id)
                when is_binary(user_id) and is_binary(version) and
                       (is_binary(room_id) or room_id == nil) and
                       (is_binary(session_id) or session_id == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :get_keys,
                {user_id, version, room_id, session_id}
              )
            end

            def delete_keys(server, user_id, version, room_id, session_id)
                when is_binary(user_id) and is_binary(version) and
                       (is_binary(room_id) or room_id == nil) and
                       (is_binary(session_id) or session_id == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :delete_keys,
                {user_id, version, room_id, session_id}
              )
            end

            def create_version(server, user_id, algorithm, auth_data)
                when is_binary(user_id) and is_binary(algorithm) and is_map(auth_data) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :create_version,
                {user_id, algorithm, auth_data}
              )
            end

            def update_version(server, user_id, version, algorithm, auth_data)
                when is_binary(user_id) and is_binary(version) and is_binary(algorithm) and
                       is_map(auth_data) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :update_version,
                {user_id, version, algorithm, auth_data}
              )
            end

            def get_version(server, user_id, version)
                when is_binary(user_id) and (is_binary(version) or version == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :get_version,
                {user_id, version}
              )
            end

            def delete_version(server, user_id, version)
                when is_binary(user_id) and is_binary(version) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.KeyBackup,
                :delete_version,
                {user_id, version}
              )
            end
          end

        Polyjuice.Server.Protocols.PubSub ->
          defimpl Polyjuice.Server.Protocols.PubSub do
            def publish(server, data, queues) when is_list(queues) do
              unquote(call).(server, Polyjuice.Server.Protocols.PubSub, :publish, {data, queues})
            end

            def subscribe(server, subscriber, queues) when is_list(queues) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :subscribe,
                {subscriber, queues}
              )
            end

            def unsubscribe(server, subscriber, queues) when is_list(queues) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :unsubscribe,
                {subscriber, queues}
              )
            end

            def await_sync(server, user_id, device_id, sync_token, timeout, filter)
                when is_binary(user_id) and is_binary(device_id) and is_integer(timeout) and
                       timeout >= 0 and is_map(filter) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :await_sync,
                {user_id, device_id, sync_token, timeout, filter}
              )
            end

            def get_sync_data_since(server, user_id, device_id, sync_token, filter)
                when is_binary(user_id) and is_binary(device_id) and
                       (is_map(sync_token) or sync_token == nil) and is_map(filter) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :get_sync_data_since,
                {user_id, device_id, sync_token, filter}
              )
            end

            def add_filter(server, user_id, filter)
                when is_binary(user_id) and is_map(filter) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :add_filter,
                {user_id, filter}
              )
            end

            def get_filter(server, user_id, filter_id)
                when is_binary(user_id) and is_binary(filter_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PubSub,
                :get_filter,
                {user_id, filter_id}
              )
            end
          end

        Polyjuice.Server.Protocols.PushRule ->
          defimpl Polyjuice.Server.Protocols.PushRule do
            def get_push_rules(server, user_id, scope)
                when is_binary(user_id) and (is_binary(scope) or scope == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PushRule,
                :get_push_rules,
                {user_id, scope}
              )
            end

            def get_push_rule(server, user_id, scope, kind, rule_id)
                when is_binary(user_id) and is_binary(scope) and is_binary(kind) and
                       is_binary(rule_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.PushRule,
                :get_push_rule,
                {user_id, scope, kind, rule_id}
              )
            end
          end

        Polyjuice.Server.Protocols.Room ->
          defimpl Polyjuice.Server.Protocols.Room do
            def create(server, state_events) when is_list(state_events) do
              unquote(call).(server, Polyjuice.Server.Protocols.Room, :create, {state_events})
            end

            def join(server, user_id, room_id, opts)
                when is_binary(user_id) and is_binary(room_id) and is_list(opts) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :join,
                {user_id, room_id, opts}
              )
            end

            def send_event(
                  server,
                  user_id,
                  device_id,
                  room_id,
                  event_type,
                  state_key,
                  txn_id,
                  content
                )
                when is_binary(user_id) and is_binary(device_id) and is_binary(room_id) and
                       is_binary(event_type) and
                       (is_binary(state_key) or state_key == nil) and
                       (is_binary(txn_id) or txn_id == nil) and is_map(content) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :send_event,
                {user_id, device_id, room_id, event_type, state_key, txn_id, content}
              )
            end

            def get_state(server, user_id, room_id, event_type, state_key)
                when is_binary(user_id) and is_binary(room_id) and
                       (is_binary(event_type) or event_type == nil) and
                       (is_binary(state_key) or state_key == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :get_state,
                {user_id, room_id, event_type, state_key}
              )
            end

            def get_messages(server, user_id, room_id, token, direction, opts)
                when is_binary(user_id) and is_binary(room_id) and is_binary(token) and
                       (direction == :forward or direction == :backward) and is_list(opts) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :get_messages,
                {user_id, room_id, token, direction, opts}
              )
            end

            def get_event(server, user_id, room_id, event_id)
                when is_binary(user_id) and is_binary(room_id) and is_binary(event_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :get_event,
                {user_id, room_id, event_id}
              )
            end

            def forget(server, user_id, room_id)
                when is_binary(user_id) and is_binary(room_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :forget,
                {user_id, room_id}
              )
            end

            def kick(server, user_id, room_id, target_user_id, reason)
                when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
                       is_binary(reason) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :kick,
                {user_id, room_id, target_user_id, reason}
              )
            end

            def invite(server, user_id, room_id, target_user_id, reason)
                when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
                       is_binary(reason) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :invite,
                {user_id, room_id, target_user_id, reason}
              )
            end

            def unban(server, user_id, room_id, target_user_id, reason)
                when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
                       is_binary(reason) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.Room,
                :unban,
                {user_id, room_id, target_user_id, reason}
              )
            end
          end

        Polyjuice.Server.Protocols.SendToDevice ->
          defimpl Polyjuice.Server.Protocols.SendToDevice do
            def send_to_device(server, user_id, device_id, txn_id, event_type, messages)
                when is_binary(user_id) and is_binary(device_id) and is_binary(txn_id) and
                       is_binary(event_type) and is_map(messages) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.SendToDevice,
                :send_to_device,
                {user_id, device_id, txn_id, event_type, messages}
              )
            end

            def mark_received(server, user_id, device_id, pos)
                when is_binary(user_id) and is_binary(device_id) and is_integer(pos) and pos >= 0 do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.SendToDevice,
                :mark_received,
                {user_id, device_id, pos}
              )
            end
          end

        Polyjuice.Server.Protocols.User ->
          defimpl Polyjuice.Server.Protocols.User do
            def login_flows(server) do
              unquote(call).(server, Polyjuice.Server.Protocols.User, :login_flows, {})
            end

            def log_in(server, credentials, opts)
                when is_tuple(credentials) and is_list(opts) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :log_in,
                {credentials, opts}
              )
            end

            def log_out(server, user_id, device_id)
                when is_binary(user_id) and is_binary(device_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :log_out,
                {user_id, device_id}
              )
            end

            def get_user_devices(server, user_id) when is_binary(user_id) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :get_user_devices,
                {user_id}
              )
            end

            def register(server, kind, username, password, log_in, opts)
                when (kind == :user or kind == :guest) and is_binary(username) and
                       is_binary(password) and is_boolean(log_in) and is_list(opts) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :register,
                {kind, username, password, log_in, opts}
              )
            end

            def change_password(server, user_id, password)
                when is_binary(user_id) and is_binary(password) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :change_password,
                {user_id, password}
              )
            end

            def deactivate(server, user_id) when is_binary(user_id) do
              unquote(call).(server, Polyjuice.Server.Protocols.User, :deactivate, {user_id})
            end

            def user_interactive_auth(server, access_token, type, endpoint, auth, request)
                when is_binary(access_token) and is_atom(type) and is_binary(endpoint) and
                       (is_map(auth) or auth == nil) and is_map(request) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :user_interactive_auth,
                {access_token, type, endpoint, auth, request}
              )
            end

            def get_device_info(server, user_id, target_user_id, device_id)
                when (is_binary(user_id) or user_id == nil) and is_binary(target_user_id) and
                       (is_binary(device_id) or device_id == :all) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :get_device_info,
                {user_id, target_user_id, device_id}
              )
            end

            def set_device_info(server, user_id, device_id, info)
                when is_binary(user_id) and is_binary(device_id) and is_map(info) do
              unquote(call).(server, Polyjuice.Server.Protocols.User, :set_device_info, {user_id})
            end

            def get_profile(server, user_id, target_user_id, property)
                when (is_binary(user_id) or user_id == nil) and is_binary(target_user_id) and
                       (is_binary(property) or property == nil) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :get_profile,
                {user_id, target_user_id, property}
              )
            end

            def set_profile(server, user_id, info) when is_binary(user_id) and is_map(info) do
              unquote(call).(
                server,
                Polyjuice.Server.Protocols.User,
                :set_profile,
                {user_id, info}
              )
            end
          end
      end
    end
  end
end
