# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.KeyBackup do
  @moduledoc """
  Functionality for server-side key backup.
  """

  @doc """
  Put keys in the backup.

  The `data` is a map from room ID to session ID to the backup data
  """
  @spec put_keys(
          server :: t(),
          user_id :: String.t(),
          version :: String.t(),
          data :: %{
            optional(String.t()) => %{String.t() => Polyjuice.Server.KeyBackup.backup_data()}
          }
        ) :: {:ok, non_neg_integer, String.t()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def put_keys(server, user_id, version, data)

  @spec get_keys(
          server :: t(),
          user_id :: String.t(),
          version :: String.t(),
          room_id :: String.t() | nil,
          session_id :: String.t() | nil
        ) ::
          {:ok,
           %{optional(String.t()) => %{String.t() => Polyjuice.Server.KeyBackup.backup_data()}}}
          | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_keys(server, user_id, version, room_id, session_id)

  @spec delete_keys(
          server :: t(),
          user_id :: String.t(),
          version :: String.t(),
          room_id :: String.t() | nil,
          session_id :: String.t() | nil
        ) :: {:ok, non_neg_integer, String.t()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def delete_keys(server, user_id, version, room_id, session_id)

  @spec create_version(
          server :: t(),
          user_id :: String.t(),
          algorithm :: String.t(),
          auth_data :: map()
        ) :: String.t()
  def create_version(server, user_id, algorithm, auth_data)

  @spec update_version(
          server :: t(),
          user_id :: String.t(),
          version :: String.t(),
          algorithm :: String.t(),
          auth_data :: map()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def update_version(server, user_id, version, algorithm, auth_data)

  @spec get_version(
          server :: t(),
          user_id :: String.t(),
          version :: String.t() | nil
        ) ::
          {:ok,
           %{
             algorithm: String.t(),
             auth_data: map(),
             count: non_neg_integer,
             etag: String.t(),
             version: String.t()
           }}
          | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_version(server, user_id, version)

  @spec delete_version(
          server :: t(),
          user_id :: String.t(),
          version :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def delete_version(server, user_id, version)
end
