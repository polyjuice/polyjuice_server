# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.SendToDevice do
  @moduledoc """
  Functionality for sending to-device messages.
  """

  @doc """
  Send to-device messages.

  `messages` is a map of user ID to device ID (or `*` for all known devices) to
  event.  All messages are sent with the same event type.

  If the `txn_id` has already been used for that user and device, then the
  request should be ignored.

  """
  @spec send_to_device(t(), String.t(), String.t(), String.t(), String.t(), %{
          String.t() => %{String.t() => map()}
        }) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def send_to_device(server, user_id, device_id, txn_id, event_type, messages)

  @doc """
  Mark to-device messages as received.

  When to-device messages are fetched from `/sync`, this function will be
  called with the stream position indicating up to which point the to-device
  messages have been delivered.  The server can forget about any previous
  to-device messages.
  """
  @spec mark_received(t(), String.t(), String.t(), non_neg_integer) :: :ok
  def mark_received(server, user_id, device_id, position)
end
