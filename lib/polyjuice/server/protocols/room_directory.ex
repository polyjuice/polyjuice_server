# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.RoomDirectory do
  @moduledoc """
  Room directory functionality.

  Any server that wishes to provide a room directory must implement this protocol.
  """

  @doc """
  Fetch the rooms in the room directory.

  Returns a map with the properties:
  - `chunk`: (required) a list of rooms in the directory, matching the query
    parameters
  - `next_batch`: a pagination token to get the next chunk of the room directory
  - `prev_batch`: a pagination token to get the previous chunk of the room
    directory
  - `total_room_count_estimate`: an estimate of the total number of matching rooms
  """
  @spec public_rooms(
          server :: Polyjuice.Server.Protocols.RoomDirectory.t(),
          source :: {:server | :user, String.t()} | :anonymous,
          third_party_instance_id :: String.t() | :all | nil,
          filter :: map | nil,
          since :: String.t() | nil,
          limit :: integer
        ) :: map()
  def public_rooms(server, source, third_party_instance_id, filter, since, limit)
end
