# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.User do
  @moduledoc """
  User management functionality.

  Any server that wishes to allow users to register and/or log in must
  implement this protocol.  Also includes some device management functions.
  """

  @doc """
  Get the supported login flows.

  Returns a list of maps with the property `"type"`, indicating the login type.
  Each map may also have additional properties, depending on the type.
  """
  @spec login_flows(server :: Polyjuice.Server.Protocols.User.t()) :: list(map())
  def login_flows(server)

  @doc """
  Log in a user.

  Recognized options are:
  - `device_id:` the requested device ID
  - `initial_device_display_name:` the display name for the device

  Otherwise, returns a map with the properties:
  - `access_token:` (required) the access token for the user
  - `device_id:` (required) the device ID associated with the session
  - `username:` (required if `user_id` is not given) the username to be used
  - `user_id:` (required if `username` is not given) the user ID to be used.
    If both `user_id` and `username` are provided, `username` will be ignored.
  - `well_known:` (optional) discovery information for the client to use
  """
  @spec log_in(
          server :: Polyjuice.Server.Protocols.User.t(),
          credentials :: {:password, String.t(), String.t()} | {:token, String.t()},
          opts :: Keyword.t()
        ) :: {:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def log_in(server, credentials, opts)

  @doc """
  Log out a device.
  """
  @spec log_out(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          device_id :: String.t()
        ) :: any
  def log_out(server, user_id, device_id)

  @doc """
  Get the logged-in devices for a user.

  If the user does not exist, it should return `[]`.
  """
  @spec get_user_devices(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t()
        ) :: list(String.t())
  def get_user_devices(server, user_id)

  @doc """
  Register a user.

  Recognized options are:
  - `device_id:` the requested device ID
  - `initial_device_display_name:` the display name for the device

  Otherwise, returns a map with the properties:
  - `access_token:` (required if `log_in` is `true`) the access token for the user
  - `device_id:` (required) the device ID associated with the session
  - `username:` (required if `user_id` is not given) the username to be used
  - `user_id:` (required if `username` is not given) the user ID to be used.
    If both `user_id` and `username` are provided, `username` will be ignored.
  """
  @spec register(
          server :: Polyjuice.Server.Protocols.User.t(),
          kind :: :user | :guest,
          username :: String.t(),
          password :: String.t(),
          log_in :: boolean,
          opts :: Keyword.t()
        ) :: {:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def register(server, kind, user_id, password, log_in, opts)

  @doc """
  Change a user's password.
  """
  @spec change_password(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          password :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def change_password(server, user_id, password)

  @doc """
  Deactivate a user.
  """
  # FIXME: need params for 3PID unbinding?
  @spec deactivate(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def deactivate(server, user_id)

  @doc """
  Process a user-interactive authentication request.

  `type` and `endpoint` allow handling different endpoints differently;
  `endpoint` gives the name of the endpoint being called (with the request
  method, without the version prefix, and without any parameters expanded),
  while `type` allows the server to handle different, but related, endpoints in
  the same way.  For example, `POST /delete_devices` and `DELETE
  /devices/{deviceId}` are both for logging out devices, so they would have the
  same `type`.  `auth` is the "auth" parameter from the request (if present),
  and `request` is the remainder of the request.

  Should return `:ok` if the user has been sufficiently authenticated and may
  proceed, or `{:cont, resp}` if the user must complete more steps, where
  `resp` is the response to return to the client.
  """
  @spec user_interactive_auth(
          server :: Polyjuice.Server.Protocols.User.t(),
          access_token :: String.t(),
          type :: atom,
          endpoint :: String.t(),
          auth :: map() | nil,
          request :: map()
        ) :: :ok | {:cont, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def user_interactive_auth(server, access_token, type, endpoint, auth, request)

  @doc """
  Get information about the device(s).

  If `device_id` is `:all`, then all of a user's devices will be returned.
  Returns a map of device ID to device info.  The device info includes:
  - device display name,
  - last seen IP address,
  - last seen timestamp.

  The last seen IP address and last seen timestamp should only be included if
  the target user ID is the same as the requesting user ID.
  """
  @spec get_device_info(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          target_user_id :: String.t(),
          device_id :: String.t() | :all
        ) ::
          {:ok, %{optional(String.t()) => map()}}
          | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_device_info(server, user_id, target_user_id, device_id)

  @doc """
  Set the information about the device.

  `info` is a map from property name to data.  Currently, only the device display
  name (`"display_name"`) can be set.  If the value is `nil`, the field should be
  deleted.
  """
  @spec set_device_info(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          device_id :: String.t(),
          info :: %{optional(String.t()) => any}
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set_device_info(server, user_id, device_id, info)

  @doc """
  Get the user's profile.

  Returns a `{:ok, profile}` where `profile` is a map from property name to
  value.  If `property` is non-`nil`, `profile` may be filtered to only contain
  the given name (if that profile property is set).
  """
  @spec get_profile(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          target_user_id :: String.t(),
          property :: String.t() | nil
        ) ::
          {:ok, %{optional(String.t()) => String.t()}}
          | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_profile(server, user_id, target_user_id, property)

  @doc """
  Set properties in the user's profile.

  `profile` is a map of profile fields to values.  If the value is `nil`, the
  field should be deleted.
  """
  @spec set_profile(
          server :: Polyjuice.Server.Protocols.User.t(),
          user_id :: String.t(),
          profile :: %{optional(String.t()) => any}
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set_profile(server, user_id, profile)
end
