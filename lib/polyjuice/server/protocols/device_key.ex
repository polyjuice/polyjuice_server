# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.DeviceKey do
  @moduledoc """
  Functionality for device keys.
  """

  @doc """
  Set the user's device keys.

  This function should also notify users about the key change.
  FIXME: explain how
  """
  @spec set_device_keys(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          device_id :: String.t(),
          device_keys :: map()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set_device_keys(server, user_id, device_id, device_keys)

  @doc """
  Add one-time keys for the device.

  `keys` is a map from key ID (prefixed with the algorithm name) to key data.

  Should return an error if one of the key IDs was previously set and is being
  set to different key from the previous value.
  """
  @spec add_one_time_keys(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          device_id :: String.t(),
          keys :: %{String.t() => any}
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def add_one_time_keys(server, user_id, device_id, keys)

  # FIXME: fallback keys

  @doc """
  Set the user's cross-signing keys.

  `keys` is a map from key type (e.g. "master_key" or "user_signing_key") to key.
  """
  @spec set_cross_signing_keys(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          keys :: %{String.t() => map()}
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set_cross_signing_keys(server, user_id, keys)

  @doc """
  Add signatures to device and cross-signing keys.

  `signatures` is a map of user ID to device ID to signatures object.

  Returns a map of user ID to device ID to error, for any keys that failed to
  be processed.
  """
  @spec add_signatures(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          signatures :: %{String.t() => %{String.t() => map()}}
        ) :: %{optional(String.t()) => %{String.t() => Jason.Encoder}}
  def add_signatures(server, user_id, signatures)

  @doc """
  Claim a one-time key.

  This function should also notify the user that the number of one-time keys has
  decreased. FIXME: explain how
  """
  @spec claim_key(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          one_time_keys :: %{String.t() => %{String.t() => String.t()}}
        ) ::
          %{optional(String.t()) => %{String.t() => %{String.t() => any}}}
  def claim_key(server, one_time_keys)

  @doc """
  Query device keys.

  Returns a map from key type (`device_keys`, `master_keys`,
  `self_signing_keys`, `user_signing_keys`) to user ID to (device ID to key), in
  the case of `device_keys`, or to key in the case of cross-signing keys.

  """
  @spec query_keys(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t() | nil,
          device_keys :: %{String.t() => list(String.t())},
          token :: non_neg_integer | nil
        ) ::
          %{String.t() => %{optional(String.t()) => map()}}
  def query_keys(server, user_id, device_keys, token)

  @doc """
  Get the users who have updated their device keys within the given sync token range.
  """
  @spec get_changes(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          from :: Polyjuice.Server.SyncToken.t(),
          to :: Polyjuice.Server.SyncToken.t() | nil
        ) :: {list(String.t()), list(String.t())}
  def get_changes(server, user_id, from, to)

  @doc """
  Get the device's key counts.

  Returns the number of one-time keys that the device has (as a map from
  algorithm to count), and a list of algorithms for which it has an unused
  fallback key.
  """
  @spec key_counts(
          server :: Polyjuice.Server.Protocols.DeviceKey.t(),
          user_id :: String.t(),
          device_id :: String.t()
        ) :: %{
          one_time_keys: %{optional(String.t()) => non_neg_integer},
          unused_fallback_keys: list(String.t())
        }
  def key_counts(server, user_id, device_id)

  # FIXME: probably will need some functions for federation
end
