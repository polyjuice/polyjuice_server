# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.KeyBackup do
  @type backup_data :: %{
          first_message_index: non_neg_integer,
          forwarded_count: non_neg_integer,
          is_verified: boolean,
          session_data: map()
        }

  @doc """
  Determine which backup data is better.

  If a client tries to back up a key that the server already has a backup of,
  the server must determine whether to keep its current data, or replace it
  with the new data.  This function can be used to determine which backup data
  is better; if the new data is better, the server should replace the current
  data with the new data, otherwise the server should keep the current data.
  """
  @spec compare(backup_data(), backup_data()) :: :lt | :eq | :gt
  def compare(
        %{first_message_index: _, forwarded_count: _, is_verified: _, session_data: _} = first,
        %{first_message_index: _, forwarded_count: _, is_verified: _, session_data: _} = second
      ) do
    cond do
      first.is_verified and not second.is_verified -> :gt
      not first.is_verified and second.is_verified -> :lt
      first.first_message_index < second.first_message_index -> :gt
      first.first_message_index > second.first_message_index -> :lt
      first.forwarded_count < second.forwarded_count -> :gt
      first.forwarded_count > second.forwarded_count -> :lt
      true -> :eq
    end
  end
end
