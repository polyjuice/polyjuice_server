# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Client do
  @moduledoc """
  Functions for making federation requests.
  """

  @spec get_server_keys(
          client :: Polyjuice.Server.Federation.Client.API.t(),
          server :: String.t()
        ) :: {:ok, map} | {:error, any}
  def get_server_keys(client, server) when is_binary(server) do
    Polyjuice.Server.Federation.Client.API.call(
      client,
      server,
      %Polyjuice.Server.Federation.Endpoint.GetKeyServer{}
    )
  end

  @spec get_server_version(
          client :: Polyjuice.Server.Federation.Client.API.t(),
          server :: String.t()
        ) :: {:ok, map} | {:error, any}
  def get_server_version(client, server) when is_binary(server) do
    Polyjuice.Server.Federation.Client.API.call(
      client,
      server,
      %Polyjuice.Server.Federation.Endpoint.GetVersion{}
    )
  end

  defprotocol API do
    @moduledoc """
    Protocol for making Matrix federation API requests.
    """

    @doc """
    Call a Matrix federation API.

    This is a lower-level function; generally, clients will want to call one of
    the higher-level functions from `Polyjuice.Server.Federation.Client` or one
    of its submodules.
    """
    @spec call(
            fed_client_api :: Polyjuice.Server.Federation.Client.API.t(),
            destination :: String.t(),
            endpoint :: Polyjuice.Server.Federation.Endpoint.Proto.t()
          ) :: any
    def call(fed_client_api, target, endpoint)
  end

  @type t :: %__MODULE__{
          server_name: String.t(),
          signing_key: Polyjuice.Util.SigningKey.t() | (() -> Polyjuice.Util.SigningKey.t()),
          discovery_cache: Polyjuice.Server.Federation.Discover.Cache.t()
        }

  defstruct [
    :server_name,
    :signing_key,
    :discovery_cache
  ]

  defimpl Polyjuice.Server.Federation.Client.API do
    def call(client, destination, endpoint) do
      httpspec = Polyjuice.Server.Federation.Endpoint.Proto.http_spec(endpoint, destination)

      path =
        if(String.starts_with?(httpspec.path, "/"), do: httpspec.path, else: "/" <> httpspec.path) <>
          if httpspec.query, do: "?" <> URI.encode_query(httpspec.query), else: ""

      headers =
        if httpspec.auth_required do
          {:ok, signature} =
            Polyjuice.Server.Federation.Authentication.sign(
              client.server_name,
              cond do
                is_function(client.signing_key) -> client.signing_key.()
                true -> client.signing_key
              end,
              httpspec.method,
              path,
              destination,
              if(httpspec.method == :post or httpspec.method == :put,
                do: httpspec.body,
                else: nil
              )
            )

          [{"Authorization", signature} | httpspec.headers]
        else
          httpspec.headers
        end

      discovery =
        Polyjuice.Server.Federation.Discover.discover(destination, client.discovery_cache)

      # FIXME:
      hackney_opts = []

      case discovery do
        nil ->
          {:error, :no_discovery}

        %{locations: locations, host: host, identity: identity} ->
          identity_charlist = String.to_charlist(identity)
          # FIXME: merge with existing ssl options if any
          hackney_opts = [
            {:ssl_options, :hackney_connection.ssl_opts(identity_charlist, [])} | hackney_opts
          ]

          headers = [{"Host", host} | headers]

          # try each location in sequence and stop with the successful
          # connection.  If no successful connection is made, return the last
          # error
          Enum.reduce_while(
            locations,
            {:error, :no_locations},
            fn {hostname, port}, _ ->
              res =
                Polyjuice.Server.Federation.Client._do_call(
                  destination,
                  hostname,
                  port,
                  httpspec,
                  path,
                  headers,
                  endpoint,
                  hackney_opts
                )

              case res do
                {:error, :econnrefused} -> {:cont, res}
                {:error, :ehostunreach} -> {:cont, res}
                {:error, :connect_timeout} -> {:cont, res}
                _ -> {:halt, res}
              end
            end
          )
      end
    end
  end

  def _do_call(destination, hostname, port, httpspec, path, headers, endpoint, hackney_opts) do
    url =
      %URI{
        scheme: "https",
        host: hostname,
        port: port,
        path: path
      }
      |> to_string()

    body =
      if httpspec.body == nil do
        ""
      else
        {:ok, json} = Polyjuice.Util.JSON.canonical_json(httpspec.body)
        json
      end

    case :hackney.request(
           httpspec.method,
           url,
           headers,
           body,
           hackney_opts
         ) do
      {:ok, status_code, resp_headers, client_ref} ->
        body =
          if httpspec.stream_response do
            Polyjuice.Server.Federation.Client.hackney_response_stream(client_ref)
          else
            {:ok, body} = :hackney.body(client_ref)
            body
          end

        # FIXME: handle any status codes specially?
        Polyjuice.Server.Federation.Endpoint.Proto.transform_http_result(
          endpoint,
          status_code,
          resp_headers,
          body,
          destination
        )

      err ->
        # anything else is an error -- return as-is
        err
    end
  end

  @doc false
  def hackney_response_stream(client_ref) do
    Stream.unfold(
      client_ref,
      fn client_ref ->
        case :hackney.stream_body(client_ref) do
          {:ok, data} -> {data, client_ref}
          _ -> nil
        end
      end
    )
  end
end
