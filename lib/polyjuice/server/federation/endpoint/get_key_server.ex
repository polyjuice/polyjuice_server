# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Endpoint.GetKeyServer do
  @moduledoc """
  Get a server's keys.

  Return `{:ok, response}` on success.  If the response does not correspond to
  the expected destination homeserver, it will return `{:error, :wrong_host}`.
  If the response is not properly signed, it will return `{:error,
  :bad_signature}`.

  https://spec.matrix.org/unstable/server-server-api/#get_matrixkeyv2serverkeyid
  """

  @type t :: %__MODULE__{}

  defstruct []

  defimpl Polyjuice.Server.Federation.Endpoint.Proto do
    def http_spec(_, _) do
      Polyjuice.Server.Federation.Endpoint.HttpSpec.get(:key_v2, "server", auth_required: false)
    end

    def transform_http_result(req, status_code, resp_headers, body, destination) do
      Polyjuice.Server.Federation.Endpoint.parse_response(
        req,
        status_code,
        resp_headers,
        body,
        destination
      )
    end
  end

  defimpl Polyjuice.Server.Federation.Endpoint.BodyParser do
    def parse(
          _,
          %{"server_name" => server_name, "verify_keys" => verify_keys} = body,
          destination
        ) do
      if server_name != destination do
        {:error, {:wrong_host, server_name}}
      else
        # make sure it's signed properly
        # FIXME: try/catch if "key" missing, or invalid
        verify_keys =
          verify_keys
          |> Enum.map(fn {id, %{"key" => key}} ->
            case id do
              <<"ed25519:", id::binary>> ->
                Polyjuice.Util.Ed25519.VerifyKey.from_base64(key, id)

              _ ->
                nil
            end
          end)
          |> Enum.filter(&(&1 != nil))

        if verify_keys != [] and
             Enum.all?(verify_keys, &Polyjuice.Util.JSON.signed?(body, destination, &1)) do
          {:ok, body}
        else
          {:error, :bad_signature}
        end
      end
    end

    def parse(_, _, _) do
      {:error, :missing_fields}
    end
  end
end
