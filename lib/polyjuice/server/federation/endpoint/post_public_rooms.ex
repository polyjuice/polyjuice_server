# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Endpoint.PostPublicRooms do
  @moduledoc """
  Get a server's public room list.

  Return `{:ok, response}` on success.

  https://spec.matrix.org/unstable/server-server-api/#post_matrixfederationv1publicrooms
  """

  @type t :: %__MODULE__{
          filter: map | nil,
          limit: integer | nil,
          since: String.t() | nil,
          third_party_instance_id: String.t() | nil | :all
        }

  defstruct [
    :filter,
    :limit,
    :since,
    :third_party_instance_id
  ]

  defimpl Polyjuice.Server.Federation.Endpoint.Proto do
    def http_spec(req, _) do
      params =
        [
          if(req.filter != nil, do: [{"filter", req.filter}], else: []),
          if(req.limit != nil, do: [{"limit", req.limit}], else: []),
          if(req.since != nil, do: [{"since", req.since}], else: []),
          case req.third_party_instance_id do
            nil -> []
            :all -> [{"include_all_networks", true}]
            _ -> [{"third_party_instance_id", req.third_party_instance_id}]
          end
        ]
        |> Enum.concat()
        |> Map.new()

      Polyjuice.Server.Federation.Endpoint.HttpSpec.post(
        :v1,
        "publicRooms",
        body: params
      )
    end

    def transform_http_result(req, status_code, resp_headers, body, destination) do
      Polyjuice.Server.Federation.Endpoint.parse_response(
        req,
        status_code,
        resp_headers,
        body,
        destination
      )
    end
  end
end
