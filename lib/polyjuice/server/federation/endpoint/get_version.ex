# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Endpoint.GetVersion do
  @moduledoc """
  Get a server's implementation name and version.

  https://spec.matrix.org/unstable/server-server-api/#get_matrixfederationv1version
  """

  @type t :: %__MODULE__{}

  defstruct []

  defimpl Polyjuice.Server.Federation.Endpoint.Proto do
    def http_spec(_, _) do
      Polyjuice.Server.Federation.Endpoint.HttpSpec.get(:v1, "version", auth_required: false)
    end

    def transform_http_result(req, status_code, resp_headers, body, destination) do
      Polyjuice.Server.Federation.Endpoint.parse_response(
        req,
        status_code,
        resp_headers,
        body,
        destination
      )
    end
  end
end
