# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Authentication do
  require Logger

  @doc ~S"""
  Produce a signature for a federation request.

  The return value is suitable to set as the `Authorization` header.
  """
  @spec sign(
          server_name :: String.t(),
          signing_key :: Polyjuice.Util.SigningKey.t(),
          method :: atom | String.t(),
          uri :: String.t(),
          destination :: String.t(),
          content :: map() | nil
        ) :: {:ok, iodata()} | :error
  def sign(server_name, signing_key, method, uri, destination, content \\ nil)
      when (is_atom(method) or is_binary(method)) and is_binary(uri) and is_binary(destination) and
             (is_map(content) or content == nil) do
    json = %{
      "method" => method |> to_string() |> String.upcase(),
      "uri" => uri,
      "origin" => server_name,
      "destination" => destination
    }

    json = if content == nil, do: json, else: Map.put(json, "content", content)

    case Polyjuice.Util.JSON.canonical_json(json) do
      {:ok, json} ->
        key_id = Polyjuice.Util.SigningKey.id(signing_key)
        signature = Polyjuice.Util.SigningKey.sign(signing_key, json)

        {:ok,
         [
           "X-Matrix origin=\"",
           server_name,
           "\",key=\"",
           key_id,
           "\",sig=\"",
           signature,
           "\",destination=\"",
           destination,
           ?"
         ]}

      _ ->
        :error
    end
  end

  @doc ~S"""
  Verify a signature from the `Authorization` header of a federation request.

  On success, returns the name of the origin server.
  """
  @spec verify(
          server :: Polyjuice.Server.Protocols.BaseFederation.t(),
          method :: atom | String.t(),
          uri :: String.t(),
          content :: map() | nil,
          authorization :: String.t()
        ) :: {:ok, String.t()} | :error
  def verify(server, method, uri, content \\ nil, authorization)
      when (is_atom(method) or is_binary(method)) and is_binary(uri) and
             (is_map(content) or content == nil) and
             is_binary(authorization) do
    destination = Polyjuice.Server.Protocols.BaseFederation.get_server_name(server)

    try do
      ["X-Matrix", sig] = String.split(authorization, ~r/ +/, parts: 2)

      %{
        "origin" => origin,
        "key" => key_id,
        "sig" => sig,
        "destination" => claimed_destination
      } = parse_sig(sig)

      if claimed_destination != nil and claimed_destination != destination,
        do: raise("Incorrect destination")

      key = get_key(server, origin, key_id)

      if key == nil, do: raise("No key found")

      json = %{
        "method" => method |> to_string() |> String.upcase(),
        "uri" => uri,
        "origin" => origin,
        "destination" => destination
      }

      json = if content == nil, do: json, else: Map.put(json, "content", content)

      case Polyjuice.Util.JSON.canonical_json(json) do
        {:ok, json} ->
          if Polyjuice.Util.VerifyKey.verify(key, json, sig) do
            {:ok, origin}
          else
            :error
          end

        _ ->
          :error
      end
    rescue
      _ -> :error
    end
  end

  defp parse_sig(sig) do
    String.split(sig, ",")
    |> Enum.map(fn param ->
      [name, value] = String.split(param, "=", parts: 2)

      value =
        case value do
          <<?", rest::binary>> ->
            String.slice(rest, 0, String.length(rest) - 1)
            |> (fn x -> Regex.replace(~r/\\(.)/, x, &String.at(&1, 1)) end).()

          _ ->
            value
        end

      {String.downcase(name), value}
    end)
    |> Map.new()
    |> Map.put_new("destination", nil)
  end

  defp get_key(server, origin, key_id) do
    now = System.system_time(:millisecond)

    case Polyjuice.Server.Protocols.BaseFederation.get_cached_remote_keys(server, origin, [key_id]) do
      %{^key_id => {key, valid_until, false}} when valid_until > now ->
        key

      _ ->
        with x when x != nil <- Polyjuice.Server.Federation.Client.API.impl_for(server),
             {:ok, %{"verify_keys" => %{^key_id => %{"key" => key}}}} <-
               Polyjuice.Server.Federation.Client.get_server_keys(server, origin) do
          Polyjuice.Util.make_verify_key(key, key_id)
        else
          _ -> nil
        end
    end
  end
end
