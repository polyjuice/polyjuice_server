# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Client.RoomDirectory do
  @doc """
  Fetch a chunk of a server's room directory.

  Possible options are:
  - `filter`: a filter to apply to the room directory.  Must be a `map` that
    has a `"generic_search_term"` key.
  - `limit`: limit the number of results returned.
  - `since`: a pagination token
  - `third_party_instance_id`: indicate whether to include third-party protocols
    in the result.  Possible values are: `:all` to include all third-party
    protocols, or a string that indicates which third-party protocol to search.

  This function requires manually paginating to retrieve the full directory by
  using the `next_batch` from the result as the next `since` pagination token.
  To retrieve the directory as a `Stream`, use `stream_public_rooms`.
  """
  def public_rooms(client, server, params \\ []) when is_binary(server) and is_list(params) do
    Polyjuice.Server.Federation.Client.API.call(
      client,
      server,
      %Polyjuice.Server.Federation.Endpoint.PostPublicRooms{
        filter: Keyword.get(params, :filter, nil),
        limit: Keyword.get(params, :limit, nil),
        since: Keyword.get(params, :since, nil),
        third_party_instance_id: Keyword.get(params, :third_party_instance_id, nil)
      }
    )
  end

  @doc """
  Query a server's room directory.

  This function returns a `Stream` of rooms.

  Possible options are:
  - `filter`: a filter to apply to the room directory.  Must be a `map` that
    has a `"generic_search_term"` key.
  - `limit`: the number of rooms to request from the server at a time.  Defaults
    to 20.
  - `third_party_instance_id`: indicate whether to include third-party protocols
    in the result.  Possible values are: `:all` to include all third-party
    protocols, or a string that indicates which third-party protocol to search.
  """
  def stream_public_rooms(client, server, params \\ [])
      when is_binary(server) and is_list(params) do
    filter = Keyword.get(params, :filter, nil)
    limit = Keyword.get(params, :limit, 20)
    third_party_instance_id = Keyword.get(params, :third_party_instance_id, nil)

    Stream.resource(
      fn -> nil end,
      fn
        :halt ->
          {:halt, nil}

        token ->
          case Polyjuice.Server.Federation.Client.API.call(
                 client,
                 server,
                 %Polyjuice.Server.Federation.Endpoint.PostPublicRooms{
                   filter: filter,
                   limit: limit,
                   since: token,
                   third_party_instance_id: third_party_instance_id
                 }
               ) do
            {:ok, res} ->
              next = Map.get(res, "next_batch")
              chunk = Map.get(res, "chunk")

              cond do
                chunk == nil -> {:halt, nil}
                next == nil -> {chunk, :halt}
                true -> {chunk, next}
              end

            _ ->
              # bail if we get any error
              {:halt, nil}
          end
      end,
      fn _ -> nil end
    )
  end
end
