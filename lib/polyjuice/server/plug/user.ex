# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.User do
  defmodule GetLogin do
    use Plug.Builder

    plug(:check_impl, builder_opts())
    plug(:login, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {200, %{"flows" => []}}}
          | opts
        ]
      )
    end

    def login(conn, opts) do
      server = opts[:server]

      flows = Polyjuice.Server.Protocols.User.login_flows(server)

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(%{"flows" => flows}))
    end
  end

  defmodule PostLogin do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:login, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def login(conn, opts) do
      server = opts[:server]
      server_name = Polyjuice.Server.Protocols.BaseClientServer.get_server_name(server)

      device_id = Map.get(conn.body_params, "device_id")
      initial_device_display_name = Map.get(conn.body_params, "initial_device_display_name")

      res =
        case conn.body_params do
          %{"type" => "m.login.password", "identifier" => identifier, "password" => password}
          when is_map(identifier) and is_binary(password) ->
            user_id =
              case identifier do
                %{"type" => "m.id.user", "user" => user} when is_binary(user) ->
                  case String.split(user, ":", parts: 2) do
                    [_] -> "@#{user}:#{server_name}"
                    [<<?@, _::binary>>, _] -> user
                  end

                # FIXME: lookup 3pids
                _ ->
                  raise Polyjuice.Server.Plug.MatrixError,
                    message: "Forbidden",
                    plug_status: 403,
                    matrix_error: ClientAPIErrors.MForbidden.new()
              end

            Polyjuice.Server.Protocols.User.log_in(
              server,
              {:password, user_id, password},
              device_id: device_id,
              initial_device_display_name: initial_device_display_name
            )

          %{"type" => "m.login.token", "token" => token} when is_binary(token) ->
            Polyjuice.Server.Protocols.User.log_in(
              server,
              {:token, token},
              device_id: device_id,
              initial_device_display_name: initial_device_display_name
            )

          # Deprecated:
          %{"type" => "m.login.password", "user" => user, "password" => password} ->
            user_id =
              case String.split(user, ":", parts: 2) do
                [_] -> "@#{user}:#{server_name}"
                [<<?@, _::binary>>, _] -> user
              end

            Polyjuice.Server.Protocols.User.log_in(
              server,
              {:password, user_id, password},
              device_id: device_id,
              initial_device_display_name: initial_device_display_name
            )

          # FIXME:
          # %{"type" => "m.login.password", "medium" => medium, "address" => address, "password" => password} ->
          # ...

          %{"type" => "m.login.password"} ->
            raise Polyjuice.Server.Plug.MatrixError,
              plug_status: 400,
              matrix_error:
                ClientAPIErrors.MBadJson.new("missing m.login.password required attributes")

          %{"type" => "m.login.token"} ->
            raise Polyjuice.Server.Plug.MatrixError,
              plug_status: 400,
              matrix_error:
                ClientAPIErrors.MBadJson.new("missing m.login.token required attributes")

          %{"type" => _} ->
            raise Polyjuice.Server.Plug.MatrixError,
              plug_status: 400,
              matrix_error: ClientAPIErrors.MUnknown.new("Unknown login type")

          %{} ->
            raise Polyjuice.Server.Plug.MatrixError,
              plug_status: 400,
              matrix_error: ClientAPIErrors.MUnknown.new("Missing login type")
        end

      case res do
        {:ok, res} ->
          res =
            case res do
              %{user_id: _} ->
                Map.delete(res, :username)

              %{username: username} ->
                res
                |> Map.delete(:username)
                |> Map.put(:user_id, "@#{username}:#{server_name}")
            end

          # deprecated property
          res =
            case res do
              %{home_server: _} ->
                res

              %{user_id: user_id} ->
                [_, home_server] = String.split(user_id, ":", parts: 2)
                Map.put(res, :home_server, home_server)
            end

          if Polyjuice.Server.Protocols.PubSub.impl_for(server) != nil do
            Polyjuice.Server.Protocols.PubSub.publish(server, :login, [
              {res.user_id, res.device_id}
            ])
          end

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(res))

        {:error, %{plug_status: status, matrix_error: err}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(status, Jason.encode!(err))
      end
    end
  end

  defmodule PostLogout do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    plug(:check_impl, builder_opts())
    plug(:logout, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def logout(conn, opts) do
      conn = fetch_query_params(conn)
      server = opts[:server]

      access_token =
        case get_req_header(conn, "authorization") do
          [<<"Bearer ", token::binary>>] ->
            String.trim(token)

          _ ->
            case Map.get(conn.query_params, "access_token") do
              nil ->
                raise Polyjuice.Server.Plug.ClientAuthentication.NotAuthenticatedError,
                  message: "Missing access token",
                  matrix_error: ClientAPIErrors.MMissingToken.new()

              token ->
                token
            end
        end

      user_info =
        Polyjuice.Server.Protocols.BaseClientServer.user_from_access_token(
          server,
          access_token
        )

      {user_id, device_id} =
        case user_info do
          {:user, user_id, device_id, _guest} ->
            {user_id, device_id}

          {:application_service, _user_id} ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Cannot log out an application service token",
              plug_status: 403,
              matrix_error: ClientAPIErrors.MForbidden.new()

          {:soft_logout, user_id, device_id, _guest} ->
            # if they were soft-logged out, log them out for real
            {user_id, device_id}

          nil ->
            raise Polyjuice.Server.Plug.MatrixError,
              # (presumably)
              message: "Already logged out",
              # so just pretend it succeeded
              plug_status: 200,
              matrix_error: %{}
        end

      # FIXME: delete device keys?

      Polyjuice.Server.Protocols.User.log_out(server, user_id, device_id)

      if Polyjuice.Server.Protocols.PubSub.impl_for(server) != nil do
        Polyjuice.Server.Protocols.PubSub.publish(server, :logout, [{user_id, device_id}])
      end

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, "{}")
    end
  end

  defmodule PostLogoutAll do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:check_impl, builder_opts())
    plug(:client_authentication, builder_opts())
    plug(:logout_all, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def logout_all(conn, opts) do
      conn = fetch_query_params(conn)
      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      # FIXME: delete device keys?
      Polyjuice.Server.Protocols.User.get_user_devices(server, user_id)
      |> Enum.each(fn device_id ->
        Polyjuice.Server.Protocols.User.log_out(server, user_id, device_id)

        if Polyjuice.Server.Protocols.PubSub.impl_for(server) != nil do
          Polyjuice.Server.Protocols.PubSub.publish(server, :logout, [{user_id, device_id}])
        end
      end)

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, "{}")
    end
  end

  defmodule GetDevices do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:check_impl, builder_opts())
    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:get_devices, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def get_devices(conn, opts) do
      server = opts[:server]
      user_id = Map.fetch!(conn.assigns, :user).user_id

      case Polyjuice.Server.Protocols.User.get_device_info(server, user_id, user_id, :all) do
        {:ok, devices} ->
          devices =
            Enum.map(devices, fn {device_id, info} -> Map.put(info, :device_id, device_id) end)

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"devices" => devices}))

        {:error, %{plug_status: status, matrix_error: err}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(status, Jason.encode!(err))
      end
    end
  end

  defmodule GetDevice do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:check_impl, builder_opts())
    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:get_device, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def get_device(conn, opts) do
      server = opts[:server]
      user_id = Map.fetch!(conn.assigns, :user).user_id
      device_id = conn.path_params["device_id"]

      case Polyjuice.Server.Protocols.User.get_device_info(server, user_id, user_id, device_id) do
        {:ok, %{^device_id => device_info}} ->
          device_info = Map.put(device_info, :device_id, device_id)

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(device_info))

        {:ok, %{}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(404, Jason.encode!(ClientAPIErrors.MNotFound.new()))

        {:error, %{plug_status: status, matrix_error: err}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(status, Jason.encode!(err))
      end
    end
  end

  defmodule GetProfile do
    alias Polyjuice.Util.ClientAPIErrors
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_optional: 2]

    plug(:check_impl, builder_opts())
    plug(:client_authentication_optional, builder_opts())
    plug(:get_profile, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.User},
          {:check_impl_default_response, {403, ClientAPIErrors.MForbidden.new()}}
          | opts
        ]
      )
    end

    def get_profile(conn, opts) do
      server = opts[:server]

      user_id =
        case Map.get(conn.assigns, :user) do
          %{user_id: user_id} -> user_id
          _ -> nil
        end

      target_user_id = conn.path_params["user_id"]

      # FIXME: handle remote users too

      case Polyjuice.Server.Protocols.User.get_profile(server, user_id, target_user_id, nil) do
        {:ok, profile} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(profile))

        {:error, %{plug_status: status, matrix_error: err}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(status, Jason.encode!(err))
      end
    end
  end
end
