# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.Room do
  defmodule PostJoin do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:join, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.Room}
          | opts
        ]
      )
    end

    def join(conn, opts) do
      # FIXME: look up alias, if given
      room_id = conn.path_params["room_id_or_alias"]

      join_room_id(room_id, conn, opts)
    end

    def join_room_id(room_id, conn, opts) do
      user_id = conn.assigns[:user].user_id

      server = opts[:server]
      conn = fetch_query_params(conn)

      opts =
        [
          case Map.fetch(conn.body_params, "reason") do
            {:ok, reason} when is_binary(reason) ->
              [reason: reason]

            {:ok, _} ->
              raise Polyjuice.Server.Plug.MatrixError,
                message: "Invalid reason",
                plug_status: 400,
                matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new("Invalid reason")

            :error ->
              []
          end,
          case Map.fetch(conn.body_params, "third_party_signed") do
            {:ok,
             %{"mxid" => mxid, "sender" => sender, "signatures" => signatures, "token" => token} =
                 third_party_signed}
            when is_binary(mxid) and is_binary(sender) and is_map(signatures) and is_binary(token) ->
              [third_party_signed: third_party_signed]

            {:ok, _} ->
              raise Polyjuice.Server.Plug.MatrixError,
                message: "Invalid third_party_signed",
                plug_status: 400,
                matrix_error:
                  Polyjuice.Util.ClientAPIErrors.MInvalidParam.new("Invalid third_party_signed")

            :error ->
              []
          end
        ]
        |> Enum.concat()

      case Polyjuice.Server.Protocols.Room.join(server, user_id, room_id, opts) do
        :ok ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"room_id" => room_id}))

        {:error, :unknown_room} ->
          # FIXME: check federation
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            404,
            Jason.encode!(Polyjuice.Util.ClientAPIErrors.MNotFound.new("Unknown room"))
          )

        {:error, %{plug_status: status, matrix_error: error}} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(status, Jason.encode!(error))
      end
    end
  end

  defmodule PostRoomsJoin do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:join, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.Room}
          | opts
        ]
      )
    end

    def join(conn, opts) do
      room_id = conn.path_params["room_id"]

      Polyjuice.Server.Plug.Room.PostJoin.join_room_id(room_id, conn, opts)
    end
  end

  defmodule PutRoomsSend do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:rooms_send, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.Room}
          | opts
        ]
      )
    end

    def rooms_send(conn, opts) do
      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      device_id = conn.assigns[:user].device_id
      room_id = conn.path_params["room_id"]
      event_type = conn.path_params["event_type"]
      txn_id = conn.path_params["txn_id"]

      case Polyjuice.Server.Protocols.Room.send_event(
             server,
             user_id,
             device_id,
             room_id,
             event_type,
             nil,
             txn_id,
             conn.body_params
           ) do
        {:ok, event_id} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"event_id" => event_id}))

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end

  defmodule PostRoomsInvite do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:rooms_invite, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.Room}
          | opts
        ]
      )
    end

    def rooms_invite(conn, opts) do
      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      room_id = conn.path_params["room_id"]

      with {:ok, target_user_id} <- Map.fetch(conn.body_params, "user_id") do
        case Polyjuice.Server.Protocols.Room.invite(
               server,
               user_id,
               room_id,
               target_user_id,
               Map.get(conn.body_params, "reason", "")
             ) do
          :ok ->
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(200, "{}")

          {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
        end
      else
        _ ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(400, Jason.encode!(Polyjuice.Util.ClientAPIErrors.MBadJson.new()))
      end
    end
  end

  defmodule GetRoomsMembers do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:get_members, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.Room}
          | opts
        ]
      )
    end

    def get_members(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      room_id = conn.path_params["room_id"]

      at = Map.get(conn.query_params, "at")
      membership = Map.get(conn.query_params, "membership")
      not_membership = Map.get(conn.query_params, "not_membership")

      # FIXME: handle "at" param

      case Polyjuice.Server.Protocols.Room.get_state(
             server,
             user_id,
             room_id,
             "m.room.member",
             nil
           ) do
        {:ok, member_events} ->
          filter =
            case {membership, not_membership} do
              {nil, nil} ->
                nil

              {nil, _} ->
                &case &1 do
                  %{"content" => %{"membership" => ^membership}} -> true
                  _ -> false
                end

              {_, nil} ->
                &case &1 do
                  %{"content" => %{"membership" => ^not_membership}} -> false
                  _ -> true
                end

              {_, _} ->
                &case &1 do
                  %{"content" => %{"membership" => ^membership}} -> true
                  %{"content" => %{"membership" => ^not_membership}} -> false
                  _ -> true
                end
            end

          member_events =
            if filter == nil do
              member_events
            else
              Enum.filter(member_events, filter)
            end

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"chunk" => member_events}))

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end
end
