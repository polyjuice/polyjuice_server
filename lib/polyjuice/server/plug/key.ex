# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.Key do
  use Plug.Router
  use Plug.ErrorHandler

  plug(:match)
  plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
  plug(:dispatch, builder_opts())

  get("/v2/server/:key_id", do: get_keys(conn, opts))
  get("/v2/server", do: get_keys(conn, opts))

  def get_keys(conn, opts) do
    server = opts[:server]
    server_name = Polyjuice.Server.Protocols.BaseFederation.get_server_name(server)
    old_keys = Polyjuice.Server.Protocols.BaseFederation.get_old_keys(server)
    keys = Polyjuice.Server.Protocols.BaseFederation.get_keys(server)

    one_week = System.system_time(:millisecond) + 604_800_000

    json = %{
      "old_verify_keys" => old_keys,
      "server_name" => server_name,
      # set validity to the earliest key validity, max one week
      "valid_until_ts" =>
        Enum.reduce(keys, one_week, fn {_, {_, _, valid_until}}, acc ->
          if valid_until != nil, do: min(valid_until, acc), else: acc
        end),
      "verify_keys" =>
        Enum.map(keys, fn {key_id, {_priv, pub, _}} ->
          {key_id, pub |> Enum.map(fn {k, v} -> {to_string(k), v} end) |> Map.new()}
        end)
        |> Map.new()
    }

    {:ok, signatures} =
      Enum.reduce(keys, {:ok, json}, fn {_key_id, {priv, _pub, _}}, {:ok, acc} ->
        Polyjuice.Util.JSON.sign(acc, server_name, priv)
      end)

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Jason.encode!(signatures))
  end

  match _ do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(400, ~S({"errcode":"M_UNRECOGNIZED","error":"Unrecognized request"}))
  end

  def handle_errors(conn, err) do
    Polyjuice.Server.Plug.Matrix.handle_errors(conn, err)
  end
end
