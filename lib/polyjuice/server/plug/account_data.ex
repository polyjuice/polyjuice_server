# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.AccountData do
  defmodule GetUserAccountData do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:get_account_data, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.AccountData}
          | opts
        ]
      )
    end

    def get_account_data(conn, opts) do
      user_id = conn.assigns[:user].user_id

      server = opts[:server]

      if user_id != conn.path_params["user_id"] do
        raise Polyjuice.Server.Plug.MatrixError,
          message: "Cannot view another user's account data",
          plug_status: 403,
          matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
      end

      room_id = conn.path_params["room_id"]
      type = conn.path_params["type"]

      case Polyjuice.Server.Protocols.AccountData.get(server, user_id, room_id, type) do
        {:ok, content} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(content))

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end

  defmodule PutUserAccountData do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:put_account_data, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.AccountData}
          | opts
        ]
      )
    end

    def put_account_data(conn, opts) do
      user_id = conn.assigns[:user].user_id

      server = opts[:server]

      if user_id != conn.path_params["user_id"] do
        raise Polyjuice.Server.Plug.MatrixError,
          message: "Cannot set another user's account data",
          plug_status: 403,
          matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
      end

      room_id = conn.path_params["room_id"]
      type = conn.path_params["type"]

      if room_id != nil and not String.starts_with?(room_id, "!") do
        raise Polyjuice.Server.Plug.MatrixError,
          message: "Invalid room ID",
          plug_status: 400,
          matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new("Invalid room ID")
      end

      case Polyjuice.Server.Protocols.AccountData.set(
             server,
             user_id,
             room_id,
             type,
             conn.body_params
           ) do
        :ok ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, "{}")

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end
end
