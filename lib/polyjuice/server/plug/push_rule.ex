# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.PushRule do
  defmodule GetPushrules do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:get, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.PushRule}
          | opts
        ]
      )
    end

    def get(conn, opts) do
      user_id = conn.assigns[:user].user_id
      server = opts[:server]

      case conn.path_params do
        %{"scope" => scope, "kind" => kind, "rule_id" => rule_id} ->
          case Polyjuice.Server.Protocols.PushRule.get_push_rule(
                 server,
                 user_id,
                 scope,
                 kind,
                 rule_id
               ) do
            {:ok, rule} ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(200, Jason.encode!(rule))

            {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
          end

        %{"scope" => scope} ->
          rules =
            Polyjuice.Server.Protocols.PushRule.get_push_rules(server, user_id, scope)
            |> Map.get(scope, %{})

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(rules))

        _ ->
          rules = Polyjuice.Server.Protocols.PushRule.get_push_rules(server, user_id, nil)

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(rules))
      end
    end
  end
end
