# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.KeyBackup do
  defmodule GetRoomKeysKeys do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:get_keys, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def get_keys(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      room_id = conn.path_params["room_id"]
      session_id = conn.path_params["session_id"]

      version =
        case Map.fetch(conn.query_params, "version") do
          {:ok, version} ->
            version

          :error ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Missing version parameter",
              plug_status: 400,
              matrix_error:
                Polyjuice.Util.ClientAPIErrors.MMissingParam.new("Missing version parameter")
        end

      keys =
        Polyjuice.Server.Protocols.KeyBackup.get_keys(
          server,
          user_id,
          version,
          room_id,
          session_id
        )

      case keys do
        {:ok, data} ->
          resp =
            cond do
              room_id == nil ->
                %{
                  "rooms" =>
                    Enum.map(data, fn {room_id, sessions} ->
                      {room_id, %{"sessions" => sessions}}
                    end)
                    |> Map.new()
                }

              session_id == nil ->
                %{
                  "sessions" => Map.get(data, room_id, %{})
                }

              true ->
                Map.get(data, room_id, %{}) |> Map.get(session_id)
            end

          if resp == nil do
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(
              404,
              Jason.encode!(Polyjuice.Util.ClientAPIErrors.MNotFound.new("Key not found"))
            )
          else
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(200, Jason.encode!(resp))
          end

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end

  defmodule PutRoomKeysKeys do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:put_keys, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def put_keys(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      room_id = conn.path_params["room_id"]
      session_id = conn.path_params["session_id"]

      version =
        case Map.fetch(conn.query_params, "version") do
          {:ok, version} ->
            version

          :error ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Missing version parameter",
              plug_status: 400,
              matrix_error:
                Polyjuice.Util.ClientAPIErrors.MMissingParam.new("Missing version parameter")
        end

      body_params =
        cond do
          room_id == nil -> conn.body_params
          session_id == nil -> %{"rooms" => %{room_id => conn.body_params}}
          true -> %{"rooms" => %{room_id => %{"sessions" => %{session_id => conn.body_params}}}}
        end

      case Map.fetch(body_params, "rooms") do
        {:ok, %{} = rooms} ->
          data =
            Enum.reduce(rooms, %{}, fn {room_id, room_info}, acc ->
              case Map.fetch(room_info, "sessions") do
                {:ok, %{} = sessions} ->
                  sessions =
                    Enum.reduce(sessions, %{}, fn {session_id, session_info}, acc ->
                      case session_info do
                        %{
                          "first_message_index" => first_message_index,
                          "forwarded_count" => forwarded_count,
                          "is_verified" => is_verified,
                          "session_data" => session_data
                        }
                        when is_integer(first_message_index) and first_message_index >= 0 and
                               is_integer(forwarded_count) and forwarded_count >= 0 and
                               is_boolean(is_verified) and is_map(session_data) ->
                          Map.put(acc, session_id, %{
                            first_message_index: first_message_index,
                            forwarded_count: forwarded_count,
                            is_verified: is_verified,
                            session_data: session_data
                          })

                        _ ->
                          raise Polyjuice.Server.Plug.MatrixError,
                            message: "Malformed key",
                            plug_status: 400,
                            matrix_error:
                              Polyjuice.Util.ClientAPIErrors.MMissingParam.new(
                                "Malformed key for room #{room_id}, session #{session_id}"
                              )
                      end
                    end)

                  Map.put(acc, room_id, sessions)

                _ ->
                  acc
              end
            end)

          case Polyjuice.Server.Protocols.KeyBackup.put_keys(server, user_id, version, data) do
            {:ok, count, etag} ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(200, Jason.encode!(%{"count" => count, "etag" => etag}))

            {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
          end

        _ ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            400,
            Jason.encode!(
              Polyjuice.Util.ClientAPIErrors.MMissingParam.new("Rooms parameter missing")
            )
          )
      end
    end
  end

  defmodule DeleteRoomKeysKeys do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:delete_keys, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def delete_keys(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      room_id = conn.path_params["room_id"]
      session_id = conn.path_params["session_id"]

      version =
        case Map.fetch(conn.query_params, "version") do
          {:ok, version} ->
            version

          :error ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Missing version parameter",
              plug_status: 400,
              matrix_error:
                Polyjuice.Util.ClientAPIErrors.MMissingParam.new("Missing version parameter")
        end

      case Polyjuice.Server.Protocols.KeyBackup.delete_keys(
             server,
             user_id,
             version,
             room_id,
             session_id
           ) do
        {:ok, count, etag} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"count" => count, "etag" => etag}))

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end

  defmodule GetRoomKeysVersion do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:get_version, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def get_version(conn, opts) do
      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      version = conn.path_params["version"]

      case Polyjuice.Server.Protocols.KeyBackup.get_version(server, user_id, version) do
        {:ok, res} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(res))

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end

  defmodule PostRoomKeysVersion do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:create_version, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def create_version(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      case conn.body_params do
        %{"algorithm" => algorithm, "auth_data" => auth_data}
        when is_binary(algorithm) and is_map(auth_data) ->
          version =
            Polyjuice.Server.Protocols.KeyBackup.create_version(
              server,
              user_id,
              algorithm,
              auth_data
            )

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"version" => version}))

        %{} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            400,
            Jason.encode!(Polyjuice.Util.ClientAPIErrors.MBadJson.new())
          )
      end
    end
  end

  defmodule PutRoomKeysVersion do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:update_version, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def update_version(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      version = conn.path_params["version"]

      case conn.body_params do
        %{"algorithm" => algorithm, "auth_data" => auth_data}
        when is_binary(algorithm) and is_map(auth_data) ->
          case Polyjuice.Server.Protocols.KeyBackup.update_version(
                 server,
                 user_id,
                 version,
                 algorithm,
                 auth_data
               ) do
            :ok ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(200, "{}")

            {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
              conn
              |> put_resp_content_type("application/json")
              |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
          end

        %{} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            400,
            Jason.encode!(Polyjuice.Util.ClientAPIErrors.MBadJson.new())
          )
      end
    end
  end

  defmodule DeleteRoomKeysVersion do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:delete_version, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.KeyBackup}
          | opts
        ]
      )
    end

    def delete_version(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      version = conn.path_params["version"]

      case Polyjuice.Server.Protocols.KeyBackup.delete_version(server, user_id, version) do
        :ok ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, "{}")

        {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
      end
    end
  end
end
