# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.DeviceKey do
  defmodule Client do
    defmodule PostKeysUpload do
      alias Polyjuice.Util.ClientAPIErrors

      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication_guests_allowed: 2]

      plug(:client_authentication_guests_allowed, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:upload, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def upload(conn, opts) do
        user_id = conn.assigns[:user].user_id
        device_id = conn.assigns[:user].device_id
        server = opts[:server]

        # get the device keys (if present) and make sure that it's properly formed
        device_keys =
          case Map.fetch(conn.body_params, "device_keys") do
            {:ok,
             %{
               "algorithms" => algs,
               "device_id" => ^device_id,
               "keys" => keys,
               "signatures" => signatures,
               "user_id" => ^user_id
             } = device_keys}
            when is_list(algs) and is_binary(device_id) and is_map(keys) and is_map(signatures) ->
              if Enum.any?(algs, &(not is_binary(&1))) do
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "Invalid algorithm",
                  plug_status: 400,
                  matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid algorithm")
              end

              verified =
                Enum.all?(keys, fn {key_id, key} ->
                  case Polyjuice.Util.make_verify_key(key, key_id) do
                    nil ->
                      true

                    verify_key ->
                      case Map.get(signatures, user_id) |> Map.get(key_id) do
                        nil -> true
                        _ -> Polyjuice.Util.JSON.signed?(device_keys, user_id, verify_key)
                      end
                  end
                end)

              if not verified do
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "Invalid signature",
                  plug_status: 400,
                  matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid signature")
              end

              # FIXME: also check cross-signing signatures?
              device_keys

            {:ok, _} ->
              raise Polyjuice.Server.Plug.MatrixError,
                message: "Invalid key",
                plug_status: 400,
                matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")

            :error ->
              nil
          end

        one_time_keys =
          case Map.fetch(conn.body_params, "one_time_keys") do
            {:ok, %{} = keys} ->
              if Enum.any?(Map.keys(keys), &(not match?([_, _], String.split(&1, ":", parts: 2)))) do
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "Invalid key ID",
                  plug_status: 400,
                  matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key ID")
              end

              # FIXME: check sig on one-time keys, if signed?
              keys

            {:ok, _} ->
              raise Polyjuice.Server.Plug.MatrixError,
                message: "Invalid key",
                plug_status: 400,
                matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")

            :error ->
              nil
          end

        # FIXME: add support for fallback keys too

        if device_keys do
          Polyjuice.Server.Protocols.DeviceKey.set_device_keys(
            server,
            user_id,
            device_id,
            device_keys
          )
        end

        if one_time_keys do
          Polyjuice.Server.Protocols.DeviceKey.add_one_time_keys(
            server,
            user_id,
            device_id,
            one_time_keys
          )
        end

        key_counts = Polyjuice.Server.Protocols.DeviceKey.key_counts(server, user_id, device_id)

        resp = %{
          "one_time_key_counts" => key_counts.one_time_keys
        }

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, Jason.encode!(resp))
      end
    end

    defmodule PostKeysQuery do
      alias Polyjuice.Util.ClientAPIErrors

      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication_guests_allowed: 2]

      plug(:client_authentication_guests_allowed, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:query, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def query(conn, opts) do
        server = opts[:server]
        servername = Polyjuice.Server.Protocols.BaseClientServer.get_server_name(server)
        user_id = conn.assigns[:user].user_id

        device_keys_request = Map.get(conn.body_params, "device_keys")

        if device_keys_request == nil do
          raise Polyjuice.Server.Plug.MatrixError,
            message: "Missing device_keys parameter",
            plug_status: 400,
            matrix_error: ClientAPIErrors.MMissingParam.new("Missing device_keys parameter")
        end

        Enum.each(device_keys_request, fn {user_id, devices} ->
          if not match?(["@" <> _, _], String.split(user_id, ":", parts: 2)) do
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Invalid user ID",
              plug_status: 400,
              matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid user ID")
          end

          if not is_list(devices) or not Enum.all?(devices, &is_binary/1) do
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Invalid user ID",
              plug_status: 400,
              matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid user ID")
          end
        end)

        token =
          case Map.fetch(conn.body_params, "token") do
            {:ok, token} ->
              case Polyjuice.Server.SyncToken.parse(token) do
                {:ok, token} ->
                  Polyjuice.Server.SyncToken.get_component(token, :k)

                _ ->
                  raise Polyjuice.Server.Plug.MatrixError,
                    message: "Invalid token",
                    plug_status: 400,
                    matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid token")
              end

            :error ->
              nil
          end

        results =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(
            server,
            user_id,
            device_keys_request,
            token
          )

        device_key_results = Map.get(results, "device_keys")

        # find out what remote users we don't already have cached
        device_key_requests_by_server =
          Enum.group_by(device_keys_request, fn {user_id, _value} ->
            if Map.has_key?(device_key_results, user_id) do
              nil
            else
              [_, target_server] = String.split(user_id, ":", parts: 2)
              if target_server == servername, do: nil, else: target_server
            end
          end)
          |> Map.delete(nil)

        # FIXME: also get cross-signing keys by server

        results =
          if device_key_requests_by_server == %{} do
            results
          else
            # FIXME: query over federation
            device_key_requests_by_server
            |> Map.keys()
            |> Enum.map(&{&1, true})
            |> Map.new()
            |> (&Map.put(results, "failures", &1)).()
          end

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, Jason.encode!(results))
      end
    end

    defmodule PostKeysClaim do
      alias Polyjuice.Util.ClientAPIErrors

      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication_guests_allowed: 2]

      plug(:client_authentication_guests_allowed, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:claim, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def claim(conn, opts) do
        server = opts[:server]

        requests_by_server =
          case Map.fetch(conn.body_params, "one_time_keys") do
            {:ok, keys} when is_map(keys) ->
              Enum.group_by(keys, fn {user_id, value} ->
                if not is_map(value) or
                     not Enum.all?(value, fn {device, alg} ->
                       is_binary(device) and is_binary(alg)
                     end) do
                  raise Polyjuice.Server.Plug.MatrixError,
                    message: "Invalid request",
                    plug_status: 400,
                    matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid request")
                end

                case String.split(user_id, ":", parts: 2) do
                  [_, server] ->
                    server

                  _ ->
                    raise Polyjuice.Server.Plug.MatrixError,
                      message: "Invalid user ID",
                      plug_status: 400,
                      matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid user ID")
                end
              end)

            _ ->
              raise Polyjuice.Server.Plug.MatrixError,
                message: "Invalid request",
                plug_status: 400,
                matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid request")
          end

        servername = Polyjuice.Server.Protocols.BaseClientServer.get_server_name(server)
        {local, remote} = Map.split(requests_by_server, [servername])

        results =
          if local == %{} do
            %{}
          else
            local
            |> Map.get(servername)
            |> Map.new()
            |> (&%{"one_time_keys" => Polyjuice.Server.Protocols.DeviceKey.claim_key(server, &1)}).()
          end

        results =
          if remote == %{} do
            results
          else
            # FIXME: query over federation
            remote
            |> Map.keys()
            |> Enum.map(&{&1, true})
            |> Map.new()
            |> (&Map.put(results, "failures", &1)).()
          end

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, Jason.encode!(results))
      end
    end

    defmodule GetKeysChanges do
      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication: 2]

      plug(:client_authentication, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:changes, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def changes(conn, opts) do
        server = opts[:server]
        user_id = conn.assigns[:user].user_id

        conn = fetch_query_params(conn)

        with {:ok, from} <- Map.fetch(conn.query_params, "from"),
             {:ok, from_token} <- Polyjuice.Server.SyncToken.parse(from),
             {:ok, to} <- Map.fetch(conn.query_params, "to"),
             {:ok, to_token} <- Polyjuice.Server.SyncToken.parse(to) do
          {changed, left} =
            Polyjuice.Server.Protocols.DeviceKey.get_changes(
              server,
              user_id,
              from_token,
              to_token
            )

          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"changed" => changed, "left" => left}))
        else
          _ ->
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(400, Jason.encode!(Polyjuice.Util.ClientAPIErrors.MMissingParam.new()))
        end
      end
    end

    defmodule PostKeysDeviceSigningUpload do
      alias Polyjuice.Util.ClientAPIErrors
      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication: 2]

      plug(:client_authentication, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:ui_auth, builder_opts())
      plug(:upload, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def ui_auth(conn, opts) do
        opts = [
          {:ui_auth_type, :device_signing_upload},
          {:ui_auth_endpoint, "POST /keys/device_signing/upload"} | opts
        ]

        Polyjuice.Server.Plug.ClientAuthentication.UserInteractiveAuth.call(conn, opts)
      end

      def upload(conn, opts) do
        server = opts[:server]
        user_id = conn.assigns[:user].user_id

        keys =
          Enum.reduce(conn.body_params, %{}, fn {name, value}, acc ->
            if String.ends_with?(name, "_key") do
              # check basic structure
              case value do
                %{"keys" => keys, "usage" => usage, "user_id" => ^user_id}
                when is_map(keys) and is_list(usage) ->
                  if not Enum.member?(usage, String.slice(name, 0, String.length(name) - 4)) do
                    raise Polyjuice.Server.Plug.MatrixError,
                      message: "Invalid key",
                      plug_status: 400,
                      matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")
                  end

                  case Map.to_list(keys) do
                    [{key_id, pub_key}] ->
                      case String.split(key_id, ":", parts: 2) do
                        [_, ^pub_key] ->
                          :ok

                        _ ->
                          raise Polyjuice.Server.Plug.MatrixError,
                            message: "Invalid key",
                            plug_status: 400,
                            matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")
                      end

                      :ok

                    _ ->
                      raise Polyjuice.Server.Plug.MatrixError,
                        message: "Invalid key",
                        plug_status: 400,
                        matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")
                  end

                  :ok

                _ ->
                  raise Polyjuice.Server.Plug.MatrixError,
                    message: "Invalid key",
                    plug_status: 400,
                    matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")
              end

              # check that non-master keys are signed
              case {name, value} do
                {"master_key", _} ->
                  :ok

                # FIXME: check sig
                {_, %{"signatures" => %{^user_id => %{}}}} ->
                  :ok

                _ ->
                  raise Polyjuice.Server.Plug.MatrixError,
                    message: "Invalid key",
                    plug_status: 400,
                    matrix_error: ClientAPIErrors.MInvalidParam.new("Invalid key")
              end

              Map.put(acc, name, value)
            else
              acc
            end
          end)

        if keys == %{} do
          raise Polyjuice.Server.Plug.MatrixError,
            message: "No keys provided",
            plug_status: 400,
            matrix_error: ClientAPIErrors.MMissingParam.new("No keys provided")
        end

        Polyjuice.Server.Protocols.DeviceKey.set_cross_signing_keys(server, user_id, keys)

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, "{}")
      end
    end

    defmodule PostKeysSignaturesUpload do
      use Plug.Builder

      import Polyjuice.Server.Plug.ClientAuthentication,
        only: [client_authentication: 2]

      plug(:client_authentication, builder_opts())
      plug(:check_impl, builder_opts())
      plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
      plug(:upload, builder_opts())

      def check_impl(conn, opts) do
        Polyjuice.Server.Plug.CheckServerImpl.call(
          conn,
          [
            {:check_impl_protocol, Polyjuice.Server.Protocols.DeviceKey}
            | opts
          ]
        )
      end

      def upload(conn, opts) do
        server = opts[:server]
        user_id = conn.assigns[:user].user_id

        # FIXME: check that the signatures are valid (if we understand the
        # algorithm and we have the signing key)

        res =
          Polyjuice.Server.Protocols.DeviceKey.add_signatures(server, user_id, conn.body_params)

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, Jason.encode!(%{"failures" => res}))
      end
    end
  end
end
