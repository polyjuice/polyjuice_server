# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.SendToDevice do
  defmodule PutSendToDevice do
    alias Polyjuice.Util.ClientAPIErrors

    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:send_to_device, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.SendToDevice}
          | opts
        ]
      )
    end

    def send_to_device(conn, opts) do
      user_id = conn.assigns[:user].user_id
      device_id = conn.assigns[:user].device_id
      server = opts[:server]

      txn_id = conn.path_params["txn_id"]
      event_type = conn.path_params["event_type"]

      case Map.fetch(conn.body_params, "messages") do
        {:ok, messages} when is_map(messages) ->
          valid =
            Enum.all?(messages, fn {_, user_messages} ->
              is_map(user_messages) and
                Enum.all?(user_messages, fn {_, message} -> is_map(message) end)
            end)

          if valid do
            case Polyjuice.Server.Protocols.SendToDevice.send_to_device(
                   server,
                   user_id,
                   device_id,
                   txn_id,
                   event_type,
                   messages
                 ) do
              :ok ->
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(200, "{}")

              {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
            end
          else
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(400, Jason.encode!(ClientAPIErrors.MInvalidParam.new()))
          end

        {:ok, _messages} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(400, Jason.encode!(ClientAPIErrors.MInvalidParam.new()))

        _ ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(400, Jason.encode!(ClientAPIErrors.MMissingParam.new()))
      end
    end
  end
end
