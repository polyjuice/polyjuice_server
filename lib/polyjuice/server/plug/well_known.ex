# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.WellKnown do
  use Plug.Router

  plug(:match)
  plug(:dispatch, builder_opts())

  options("/client", do: send_resp(conn, 204, ""))

  get "/client" do
    server = opts[:server]

    # add CORS headers
    conn = Polyjuice.Server.Plug.Client.cors(conn, opts)

    with x when x != nil <- Polyjuice.Server.Protocols.Discovery.impl_for(server),
         addr when addr != nil <- Polyjuice.Server.Protocols.Discovery.address(server) do
      identity_server = Polyjuice.Server.Protocols.Discovery.identity_server(server)
      extra_fields = Polyjuice.Server.Protocols.Discovery.extra_fields(server)

      well_known =
        (extra_fields || %{})
        |> (&(if identity_server do
                Map.put(&1, "m.identity_server", %{"base_url" => to_string(identity_server)})
              else
                &1
              end)).()
        |> Map.put("m.homeserver", %{"base_url" => to_string(addr)})

      conn
      |> put_resp_content_type("application/json")
      |> put_resp_header("cache-control", "max-age=3600, public")
      |> send_resp(200, Jason.encode!(well_known))
    else
      _ ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(404, "Discovery is not supported.")
    end
  end

  get "/server" do
    server = opts[:server]

    with x when x != nil <- Polyjuice.Server.Protocols.Discovery.impl_for(server),
         delegated when delegated != nil <-
           Polyjuice.Server.Protocols.Discovery.delegated_hostname(server) do
      delegated =
        case delegated do
          %URI{} -> "#{delegated.host}:#{delegated.port}"
          _ when is_binary(delegated) -> delegated
        end

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(%{"m.server" => delegated}))
    else
      _ ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(404, "Discovery is not supported.")
    end
  end

  match _ do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(404, ~S({"errcode":"M_NOT_FOUND,"error":"Not found"}))
  end
end
