# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.Federation do
  use Plug.Router
  use Plug.ErrorHandler

  plug(:match)
  plug(:dispatch, builder_opts())

  get "/v1/publicRooms" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.RoomDirectory.Federation, opts)
  end

  post "/v1/publicRooms" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.RoomDirectory.Federation, opts)
  end

  match _ do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(400, ~S({"errcode":"M_UNRECOGNIZED","error":"Unrecognized request"}))
  end

  def handle_errors(conn, err) do
    Polyjuice.Server.Plug.Matrix.handle_errors(conn, err)
  end
end
