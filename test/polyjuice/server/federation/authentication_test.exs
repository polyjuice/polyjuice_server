# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.AuthenticationTest do
  use ExUnit.Case
  doctest Polyjuice.Server.Federation.Authentication

  test "signs and verifies a request" do
    {:ok, authorization} =
      Polyjuice.Server.Federation.Authentication.sign(
        "example.org",
        Polyjuice.Util.Ed25519.SigningKey.from_base64(
          "6CeLoNzw31x/LeF0uYpZhZEwGGPQwEHHXonZ1qrfYBM",
          "1"
        ),
        :get,
        "/_matrix/key/v2/server",
        "example.net",
        nil
      )

    server = %DummyFederationServer{
      keys: %{},
      old_keys: %{},
      remote_keys: %{
        "example.org" => %{
          "ed25519:1" =>
            {Polyjuice.Util.Ed25519.VerifyKey.from_base64(
               "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0",
               "1"
             ), System.system_time(:millisecond) + 604_800_000, false}
        }
      },
      server_name: "example.net"
    }

    authorization = IO.iodata_to_binary(authorization)

    {:ok, "example.org"} =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization
      )

    [authorization_without_destination | _] = String.split(authorization, ",destination")
    assert authorization != authorization_without_destination

    {:ok, "example.org"} =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization_without_destination
      )

    authorization_with_uppercase_names =
      Regex.replace(~r/[a-z]+=/, authorization, &String.upcase/1)

    assert authorization != authorization_with_uppercase_names

    {:ok, "example.org"} =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization_with_uppercase_names
      )

    authorization_with_extra_spaces = String.replace(authorization, "X-Matrix", "X-Matrix  ")
    assert authorization != authorization_with_extra_spaces

    {:ok, "example.org"} =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization_with_extra_spaces
      )

    authorization_with_backslashes = String.replace(authorization, "ed25519:1", "ed25519\\:1")
    assert authorization != authorization_with_backslashes

    {:ok, "example.org"} =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization_with_backslashes
      )

    # unknown key
    authorization_with_unknown_key = String.replace(authorization, "ed25519:1", "ed25519:2")
    assert authorization != authorization_with_unknown_key

    :error =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization_with_unknown_key
      )

    # wrong request method
    :error =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :delete,
        "/_matrix/key/v2/server",
        nil,
        authorization
      )

    # wrong endpoint
    :error =
      Polyjuice.Server.Federation.Authentication.verify(
        server,
        :get,
        "/_matrix/key/v2/server/foo",
        nil,
        authorization
      )

    # wrong destination
    server2 = %DummyFederationServer{
      keys: %{},
      old_keys: %{},
      remote_keys: %{
        "example.org" => %{
          "ed25519:1" =>
            {Polyjuice.Util.Ed25519.VerifyKey.from_base64(
               "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0",
               "1"
             ), System.system_time(:millisecond) + 604_800_000, false}
        }
      },
      server_name: "example.com"
    }

    :error =
      Polyjuice.Server.Federation.Authentication.verify(
        server2,
        :get,
        "/_matrix/key/v2/server",
        nil,
        authorization
      )
  end
end
