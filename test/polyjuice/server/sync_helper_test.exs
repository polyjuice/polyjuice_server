# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.SyncHelperTest do
  use ExUnit.Case

  defmodule Server do
    defstruct [
      :sync_helper,
      :agent,
      sync_data: %{}
    ]

    defimpl Polyjuice.Server.Protocols.PubSub do
      def publish(%{agent: pid}, data, queues) do
        subscribers = Agent.get(pid, & &1)

        Enum.each(queues, fn queue ->
          Map.get(subscribers, queue, [])
          |> Enum.each(&Polyjuice.Server.Protocols.Subscriber.notify(&1, queue, data))
        end)
      end

      def subscribe(%{agent: pid}, subscriber, queues) do
        Agent.update(pid, fn subscribers ->
          Enum.reduce(queues, subscribers, fn queue, acc ->
            new_queue_subscribers =
              case Map.fetch(acc, queue) do
                {:ok, queue_subscribers} ->
                  MapSet.put(queue_subscribers, subscriber)

                :error ->
                  MapSet.new([subscriber])
              end

            Map.put(acc, queue, new_queue_subscribers)
          end)
        end)
      end

      def unsubscribe(%{agent: pid}, subscriber, queues) do
        Agent.update(pid, fn subscribers ->
          Enum.reduce(queues, subscribers, fn queue, acc ->
            case Map.fetch(acc, queue) do
              {:ok, queue_subscribers} ->
                new_queue_subscribers = MapSet.delete(queue_subscribers, subscriber)
                Map.put(acc, queue, new_queue_subscribers)

              :error ->
                acc
            end
          end)
        end)
      end

      def await_sync(
            %{sync_helper: sync_helper} = server,
            user_id,
            device_id,
            sync_token,
            timeout,
            filter
          ) do
        GenServer.call(
          sync_helper,
          {:sync, server, user_id, device_id, sync_token, timeout, filter}
        )
      end

      def get_sync_data_since(%{sync_data: sync_data}, user_id, device_id, sync_token, filter) do
        sync_data
        |> Map.get(user_id, %{})
        |> Map.get(device_id, %{})
        |> Map.get(sync_token, {nil, %{}})

        # FIXME: apply filter
      end
    end
  end

  test "gets waiting data" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid,
      sync_data: %{
        "@alice:example.org" => %{
          "ADEVICE" => %{
            nil => :logout
          }
        }
      }
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == :logout

    # FIXME: kill processes
  end

  test "times out" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == {nil, %{}}

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             500,
             %{}
           ) == {nil, %{}}

    # FIXME: kill processes
  end

  test "notifies waiting clients of logout_all" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid
    }

    task =
      Task.start(fn ->
        Process.sleep(100)
        Polyjuice.Server.Protocols.PubSub.publish(server, :logout_all, ["@alice:example.org"])
      end)

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             2000,
             %{}
           ) == :logout
  end

  test "notifies waiting clients of logout" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid
    }

    task =
      Task.start(fn ->
        Process.sleep(100)

        Polyjuice.Server.Protocols.PubSub.publish(server, :logout, [
          {"@alice:example.org", "ADEVICE"}
        ])
      end)

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             2000,
             %{}
           ) == :logout
  end

  test "queues account data" do
    # The user does two syncs, receiving events in between the syncs.
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)
    {:ok, token} = Polyjuice.Server.SyncToken.parse("a123")

    data1 = %{
      "account_data" => %{
        "events" => [%{"type" => "org.example.custom", "content" => %{"foo" => "bar"}}]
      }
    }

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid,
      sync_data: %{
        "@alice:example.org" => %{
          "ADEVICE" => %{
            nil => {token, data1}
          }
        }
      }
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == {token, data1}

    data2 = %{"type" => "org.example.custom", "content" => %{"foo" => "baz"}}

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:account_data, 124, data2},
      ["@alice:example.org"]
    )

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             token,
             0,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{a: 124}},
              %{"account_data" => %{"events" => [data2]}}}
  end

  test "handles soft logout and login" do
    # The user gets soft logged out, receives events, and logs in again.
    # They should get the events that were sent between soft logout and re-login
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)
    {:ok, token} = Polyjuice.Server.SyncToken.parse("a123")

    data1 = %{
      "account_data" => %{
        "events" => [%{"type" => "org.example.custom", "content" => %{"foo" => "bar"}}]
      }
    }

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid,
      sync_data: %{
        "@alice:example.org" => %{
          "ADEVICE" => %{
            nil => {token, data1}
          }
        }
      }
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == {token, data1}

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      :soft_logout,
      ["@alice:example.org"]
    )

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             token,
             0,
             %{}
           ) == :soft_logout

    data2 = %{"type" => "org.example.custom", "content" => %{"foo" => "baz"}}

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:account_data, 124, data2},
      ["@alice:example.org"]
    )

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      :login,
      ["@alice:example.org"]
    )

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             token,
             0,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{a: 124}},
              %{"account_data" => %{"events" => [data2]}}}
  end

  test "notifies waiting clients of account data" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid
    }

    data2 = %{"type" => "org.example.custom", "content" => %{"foo" => "baz"}}

    task =
      Task.start(fn ->
        Process.sleep(100)

        Polyjuice.Server.Protocols.PubSub.publish(
          server,
          {:account_data, 124, data2},
          ["@alice:example.org"]
        )
      end)

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             2000,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{a: 124}},
              %{"account_data" => %{"events" => [data2]}}}
  end

  test "notifies waiting clients of room events" do
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid
    }

    events = [
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "foo"},
        "event_id" => "$foobar",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      }
    ]

    task =
      Task.start(fn ->
        Process.sleep(100)

        Polyjuice.Server.Protocols.PubSub.publish(
          server,
          {:room_events, 124, "!a_room", events},
          ["@alice:example.org"]
        )
      end)

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             2000,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{r: 124}},
              %{
                "rooms" => %{
                  "join" => %{
                    "!a_room" => %{
                      "state" => %{"events" => []},
                      "timeline" => %{
                        "events" => events,
                        "limited" => false,
                        "prev_batch" => "r124.0"
                      }
                    }
                  }
                }
              }}
  end

  test "queues room events" do
    # The user does two syncs, receiving events in between the syncs.
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)
    {:ok, token} = Polyjuice.Server.SyncToken.parse("r123")

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid,
      sync_data: %{
        "@alice:example.org" => %{
          "ADEVICE" => %{
            nil => {token, %{}}
          }
        }
      }
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == {token, %{}}

    events1 = [
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "1"},
        "event_id" => "$event1",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "2"},
        "event_id" => "$event2",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "3"},
        "event_id" => "$event3",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      }
    ]

    events2 = [
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "4"},
        "event_id" => "$event4",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "5"},
        "event_id" => "$event5",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "6"},
        "event_id" => "$event6",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "7"},
        "event_id" => "$event7",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "8"},
        "event_id" => "$event8",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "7"},
        "event_id" => "$event7",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "10"},
        "event_id" => "$event10",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      }
    ]

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:room_events, 124, "!a_room", events1},
      ["@alice:example.org"]
    )

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:room_events, 125, "!a_room", events2},
      ["@alice:example.org"]
    )

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             token,
             0,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{r: 125}},
              %{
                "rooms" => %{
                  "join" => %{
                    "!a_room" => %{
                      "state" => %{"events" => []},
                      "timeline" => %{
                        "events" => Enum.concat([events1, events2]),
                        "limited" => false,
                        "prev_batch" => "r124.0"
                      }
                    }
                  }
                }
              }}
  end

  test "event queueing: later state events replace old ones in states property" do
    # The user does two syncs, receiving events in between the syncs.
    {:ok, helper_pid} = GenServer.start(Polyjuice.Server.SyncHelper, [])
    {:ok, agent_pid} = Agent.start(fn -> %{} end)
    {:ok, token} = Polyjuice.Server.SyncToken.parse("r123")

    server = %Server{
      sync_helper: helper_pid,
      agent: agent_pid,
      sync_data: %{
        "@alice:example.org" => %{
          "ADEVICE" => %{
            nil => {token, %{}}
          }
        }
      }
    }

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             nil,
             0,
             %{}
           ) == {token, %{}}

    events1 = [
      %{
        "type" => "m.room.member",
        "state_key" => "@carol:example.org",
        "content" => %{"membership" => "join"},
        "event_id" => "$satetevent1",
        "room_id" => "!a_room",
        "sender" => "@carol:example.org"
      },
      %{
        "type" => "m.room.member",
        "state_key" => "@carol:example.org",
        "content" => %{"membership" => "leave"},
        "event_id" => "$stateevent2",
        "room_id" => "!a_room",
        "sender" => "@carol:example.org"
      },
      %{
        "type" => "m.room.topic",
        "state_key" => "",
        "content" => %{"topic" => "Hi"},
        "event_id" => "$stateevent3",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      }
    ]

    events2 = [
      %{
        "type" => "m.room.topic",
        "state_key" => "",
        "content" => %{"topic" => "Hello world!"},
        "event_id" => "$stateevent4",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.name",
        "state_key" => "@alice:example.org",
        "content" => %{"name" => "Hello world!"},
        "event_id" => "$stateevent5",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "1"},
        "event_id" => "$event1",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "2"},
        "event_id" => "$event2",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "3"},
        "event_id" => "$event3",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "4"},
        "event_id" => "$event4",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "5"},
        "event_id" => "$event5",
        "room_id" => "!a_room",
        "sender" => "@alice:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "6"},
        "event_id" => "$event6",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "7"},
        "event_id" => "$event7",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "8"},
        "event_id" => "$event8",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "7"},
        "event_id" => "$event7",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "10"},
        "event_id" => "$event10",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      },
      %{
        "type" => "m.room.message",
        "content" => %{"msgtype" => "m.text", "body" => "11"},
        "event_id" => "$event11",
        "room_id" => "!a_room",
        "sender" => "@bob:example.org"
      }
    ]

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:room_events, 124, "!a_room", events1},
      ["@alice:example.org"]
    )

    Polyjuice.Server.Protocols.PubSub.publish(
      server,
      {:room_events, 125, "!a_room", events2},
      ["@alice:example.org"]
    )

    assert Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             "@alice:example.org",
             "ADEVICE",
             token,
             0,
             %{}
           ) ==
             {%Polyjuice.Server.SyncToken{components: %{r: 125}},
              %{
                "rooms" => %{
                  "join" => %{
                    "!a_room" => %{
                      "state" => %{
                        "events" => [
                          %{
                            "type" => "m.room.member",
                            "state_key" => "@carol:example.org",
                            "content" => %{"membership" => "leave"},
                            "event_id" => "$stateevent2",
                            "room_id" => "!a_room",
                            "sender" => "@carol:example.org"
                          },
                          %{
                            "type" => "m.room.topic",
                            "state_key" => "",
                            "content" => %{"topic" => "Hello world!"},
                            "event_id" => "$stateevent4",
                            "room_id" => "!a_room",
                            "sender" => "@alice:example.org"
                          },
                          %{
                            "type" => "m.room.name",
                            "state_key" => "@alice:example.org",
                            "content" => %{"name" => "Hello world!"},
                            "event_id" => "$stateevent5",
                            "room_id" => "!a_room",
                            "sender" => "@alice:example.org"
                          }
                        ]
                      },
                      "timeline" => %{
                        "events" => Enum.drop(events2, 3),
                        "limited" => true,
                        "prev_batch" => "r125.3"
                      }
                    }
                  }
                }
              }}
  end

  test "puts room in 'leave' when the user leaves" do
    # FIXME:
  end
end
