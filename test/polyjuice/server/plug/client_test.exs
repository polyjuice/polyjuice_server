# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.ClientTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "returns versions" do
    conn = conn(:get, "/_matrix/client/versions")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{},
          server_name: "example.org"
        }
      )

    {status, _, _} = sent_resp(conn)

    assert status == 200
  end

  test "returns whoami (normal user)" do
    conn =
      conn(:get, "/_matrix/client/r0/account/whoami")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@alice:example.org", "ADEVICE", false}
          },
          server_name: "example.org"
        }
      )

    assert Jason.decode!(conn.resp_body) == %{
             "user_id" => "@alice:example.org",
             "device_id" => "ADEVICE"
           }
  end

  test "returns whoami (guest)" do
    conn =
      conn(:get, "/_matrix/client/r0/account/whoami")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@guest123:example.org", "ADEVICE", true}
          },
          server_name: "example.org"
        }
      )

    assert Jason.decode!(conn.resp_body) == %{
             "user_id" => "@guest123:example.org",
             "device_id" => "ADEVICE",
             "org.matrix.msc3069.is_guest" => true
           }
  end

  test "returns whoami (v3)" do
    conn =
      conn(:get, "/_matrix/client/v3/account/whoami")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@guest123:example.org", "ADEVICE", true}
          },
          server_name: "example.org"
        }
      )

    assert Jason.decode!(conn.resp_body) == %{
             "user_id" => "@guest123:example.org",
             "device_id" => "ADEVICE",
             "org.matrix.msc3069.is_guest" => true
           }
  end

  test "returns whoami (appservice)" do
    conn =
      conn(:get, "/_matrix/client/r0/account/whoami")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:application_service, "@appservice:example.org"}
          },
          server_name: "example.org"
        }
      )

    assert Jason.decode!(conn.resp_body) == %{"user_id" => "@appservice:example.org"}
  end
end
