# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.ClientAuthenticationTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Foo do
    use Plug.Router

    plug(:match)
    plug(Polyjuice.Server.Plug.ClientAuthentication, builder_opts())
    plug(:dispatch, builder_opts())

    get "/_matrix/foo/v1/bar" do
      send_resp(
        conn,
        200,
        "#{conn.assigns[:access_token]},#{conn.assigns[:user][:user_id]},#{
          conn.assigns[:user][:device_id]
        }"
      )
    end
  end

  defmodule Bar do
    use Plug.Router

    plug(:match)
    plug(Polyjuice.Server.Plug.ClientAuthentication.GuestsAllowed, builder_opts())
    plug(:dispatch, builder_opts())

    get "/_matrix/foo/v1/bar" do
      send_resp(
        conn,
        200,
        "#{conn.assigns[:access_token]},#{conn.assigns[:user][:user_id]},#{
          conn.assigns[:user][:device_id]
        }"
      )
    end
  end

  test "authentication check gets token from header" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@alice:example.org", "ADEVICE", false}
          },
          server_name: "example.org"
        }
      )

    assert conn.resp_body == "abcdefg,@alice:example.org,ADEVICE"
  end

  test "authentication check gets token from query param" do
    conn = conn(:get, "/_matrix/foo/v1/bar?access_token=abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@alice:example.org", "ADEVICE", false}
          },
          server_name: "example.org"
        }
      )

    assert conn.resp_body == "abcdefg,@alice:example.org,ADEVICE"
  end

  test "raises an error if request is not authorized" do
    conn = conn(:get, "/_matrix/foo/v1/bar")

    assert_raise Polyjuice.Server.Plug.ClientAuthentication.NotAuthenticatedError, fn ->
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{},
          server_name: "example.org"
        }
      )
    end
  end

  test "raises an error if access token not found" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar")
      |> put_req_header("authorization", "Bearer abcdefg")

    assert_raise Polyjuice.Server.Plug.ClientAuthentication.NotAuthenticatedError, fn ->
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{},
          server_name: "example.org"
        }
      )
    end
  end

  test "authentication check allows appservices to spoof users in their namespace" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar?user_id=@bob:example.org")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:application_service, "@appservice:example.org"}
          },
          appservice_users: %{
            "@appservice:example.org" => ["@bob:example.org"]
          },
          server_name: "example.org"
        }
      )

    assert conn.resp_body == "abcdefg,@bob:example.org,"
  end

  test "raises an error if AS asserts a wrong user" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar?user_id=@carol:example.org")
      |> put_req_header("authorization", "Bearer abcdefg")

    assert_raise Polyjuice.Server.Plug.ClientAuthentication.NotAuthenticatedError, fn ->
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:application_service, "@appservice:example.org"}
          },
          appservice_users: %{
            "@appservice:example.org" => ["@bob:example.org"]
          },
          server_name: "example.org"
        }
      )
    end
  end

  test "GuestsAllowed allows guests" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.Bar.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@alice:example.org", "ADEVICE", true}
          },
          server_name: "example.org"
        }
      )

    assert conn.resp_body == "abcdefg,@alice:example.org,ADEVICE"
  end

  test "non-GuestsAllowed does not allow guests" do
    conn =
      conn(:get, "/_matrix/foo/v1/bar")
      |> put_req_header("authorization", "Bearer abcdefg")

    assert_raise Polyjuice.Server.Plug.ClientAuthentication.NotAuthenticatedError, fn ->
      Polyjuice.Server.Plug.ClientAuthenticationTest.Foo.call(
        conn,
        server: %DummyClientServer{
          access_tokens: %{
            "abcdefg" => {:user, "@alice:example.org", "ADEVICE", true}
          },
          appservice_users: %{
            "@appservice:example.org" => ["@bob:example.org"]
          },
          server_name: "example.org"
        }
      )
    end
  end

  defmodule UIAuthPlug do
    use Plug.Router

    plug(:match)
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(Polyjuice.Server.Plug.ClientAuthentication, builder_opts())
    plug(:ui_auth, builder_opts())
    plug(:dispatch, builder_opts())

    def ui_auth(conn, opts) do
      opts = [{:ui_auth_type, :foo}, {:ui_auth_endpoint, "/bar"} | opts]
      Polyjuice.Server.Plug.ClientAuthentication.UserInteractiveAuth.call(conn, opts)
    end

    post "/_matrix/foo/v1/bar" do
      send_resp(
        conn,
        200,
        "OK"
      )
    end
  end

  defmodule UIAuthServer do
    defstruct []

    defimpl Polyjuice.Server.Protocols.BaseClientServer do
      def get_server_name(_) do
        "example.org"
      end

      def user_from_access_token(_, "abcdefg") do
        {:user, "@alice:example.org", "ADEVICE", false}
      end

      def user_from_access_token(_, _), do: nil

      def user_in_appservice_namespace?(_, _, _), do: false
    end

    defimpl Polyjuice.Server.Protocols.User do
      alias Polyjuice.Util.ClientAPIErrors

      def login_flows(_) do
        [%{"type" => "m.login.password"}, %{"type" => "m.login.token"}]
      end

      def log_in(_, _, _) do
        {:error,
         %Polyjuice.Server.Plug.MatrixError{
           message: "Forbidden",
           plug_status: 403,
           matrix_error: ClientAPIErrors.MForbidden.new("Incorrect password")
         }}
      end

      def log_out(_, "@alice:example.org", "ADEVICE"), do: nil

      def log_out(_, _, _) do
        raise "Unknown user/device"
      end

      def get_user_devices(_, "@alice:example.org"), do: ["ADEVICE"]

      def get_user_devices(_, _) do
        raise "Unknown user"
      end

      def register(_, _, _, _, _, _) do
        {:error,
         %Polyjuice.Server.Plug.MatrixError{
           message: "Forbidden",
           plug_status: 403,
           matrix_error: ClientAPIErrors.MForbidden.new()
         }}
      end

      def change_password(_, _, _), do: nil

      def deactivate(_, _), do: nil

      def user_interactive_auth(_, "abcdefg", _type, _endpoint, nil, _request) do
        {:cont, %{"flows" => [%{"stages" => ["m.login.dummy"]}], "session" => "session_id"}}
      end

      def user_interactive_auth(
            _,
            "abcdefg",
            _type,
            _endpoint,
            %{"type" => "m.login.dummy", "session" => "session_id"},
            _request
          ) do
        :ok
      end
    end
  end

  test "UI Auth" do
    conn =
      conn(:post, "/_matrix/foo/v1/bar", "{}")
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.UIAuthPlug.call(
        conn,
        server: %Polyjuice.Server.Plug.ClientAuthenticationTest.UIAuthServer{}
      )

    assert conn.status == 401

    assert Jason.decode!(conn.resp_body) == %{
             "flows" => [%{"stages" => ["m.login.dummy"]}],
             "session" => "session_id"
           }

    conn =
      conn(
        :post,
        "/_matrix/foo/v1/bar",
        ~S({"auth":{"type":"m.login.dummy","session":"session_id"}})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer abcdefg")

    conn =
      Polyjuice.Server.Plug.ClientAuthenticationTest.UIAuthPlug.call(
        conn,
        server: %Polyjuice.Server.Plug.ClientAuthenticationTest.UIAuthServer{}
      )

    assert conn.status == 200
    assert conn.resp_body == "OK"
  end
end
