# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.KeyTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "gets server keys" do
    conn = conn(:get, "/_matrix/key/v2/server")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyFederationServer{
          keys: %{
            "ed25519:1" =>
              {Polyjuice.Util.Ed25519.SigningKey.from_base64(
                 "6CeLoNzw31x/LeF0uYpZhZEwGGPQwEHHXonZ1qrfYBM",
                 "1"
               ), %{key: "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0"},
               System.system_time(:millisecond) + 604_800_000}
          },
          # FIXME:
          old_keys: %{},
          remote_keys: %{},
          server_name: "example.org"
        }
      )

    resp_body = Jason.decode!(conn.resp_body)
    assert Map.fetch!(resp_body, "server_name") == "example.org"

    assert Map.fetch!(resp_body, "verify_keys") == %{
             "ed25519:1" => %{"key" => "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0"}
           }

    assert Polyjuice.Util.JSON.signed?(
             resp_body,
             "example.org",
             Polyjuice.Util.Ed25519.VerifyKey.from_base64(
               "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0",
               "1"
             )
           ) == true
  end
end
