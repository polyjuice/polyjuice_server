# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.RoomDirectory.ClientTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "returns default response when protocol not implemented" do
    conn = conn(:get, "/_matrix/client/r0/publicRooms")

    assert_raise Plug.Conn.WrapperError, fn ->
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DummyFederationServer{
          keys: %{},
          old_keys: %{},
          remote_keys: %{},
          server_name: "example.org"
        }
      )
    end

    {status, _, body} = sent_resp(conn)

    assert status == 200
    assert Jason.decode!(body) == %{"chunk" => [], "total_room_count_estimate" => 0}
  end

  defmodule DirectoryServer do
    defstruct []

    defimpl Polyjuice.Server.Protocols.BaseClientServer do
      def get_server_name(_) do
        "example.org"
      end

      def user_from_access_token(_, _), do: nil

      def user_in_appservice_namespace?(_, _, _), do: false
    end

    defimpl Polyjuice.Server.Protocols.RoomDirectory do
      def public_rooms(_, _source, _third_party_instance_id, _filter, _since, _limit) do
        %{
          "chunk" => [
            %{
              "guest_can_join" => false,
              "num_joined_members" => 10,
              "name" => "Foo bar",
              "room_id" => "!foo:bar",
              "world_readable" => true
            }
          ],
          "total_room_count_estimate" => 1
        }
      end
    end
  end

  test "returns directory response" do
    conn = conn(:get, "/_matrix/client/r0/publicRooms")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %DirectoryServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "chunk" => [
               %{
                 "guest_can_join" => false,
                 "num_joined_members" => 10,
                 "name" => "Foo bar",
                 "room_id" => "!foo:bar",
                 "world_readable" => true
               }
             ],
             "total_room_count_estimate" => 1
           }
  end
end
