# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.FederationAuthenticationTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Foo do
    use Plug.Router

    plug(:match)
    plug(Polyjuice.Server.Plug.FederationAuthentication, builder_opts())
    plug(:dispatch, builder_opts())

    get "/_matrix/foo/v1/bar" do
      send_resp(conn, 200, conn.assigns[:origin])
    end
  end

  test "authentication check gets origin" do
    {:ok, authorization} =
      Polyjuice.Server.Federation.Authentication.sign(
        "example.net",
        Polyjuice.Util.Ed25519.SigningKey.from_base64(
          "6CeLoNzw31x/LeF0uYpZhZEwGGPQwEHHXonZ1qrfYBM",
          "1"
        ),
        :get,
        "/_matrix/foo/v1/bar",
        "example.org"
      )

    conn =
      conn(:get, "/_matrix/foo/v1/bar")
      |> put_req_header("authorization", IO.iodata_to_binary(authorization))

    conn =
      Polyjuice.Server.Plug.FederationAuthenticationTest.Foo.call(
        conn,
        server: %DummyFederationServer{
          keys: %{},
          old_keys: %{},
          remote_keys: %{
            "example.net" => %{
              "ed25519:1" =>
                {Polyjuice.Util.Ed25519.VerifyKey.from_base64(
                   "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0",
                   "1"
                 ), System.system_time(:millisecond) + 604_800_000, false}
            }
          },
          server_name: "example.org"
        }
      )

    assert conn.resp_body == "example.net"
  end

  test "raises an error if request is not signed" do
    conn = conn(:get, "/_matrix/foo/v1/bar")

    assert_raise Polyjuice.Server.Plug.FederationAuthentication.NotAuthenticatedError, fn ->
      Polyjuice.Server.Plug.FederationAuthenticationTest.Foo.call(
        conn,
        server: %DummyFederationServer{
          keys: %{},
          old_keys: %{},
          remote_keys: %{
            "example.net" => %{
              "ed25519:1" =>
                {Polyjuice.Util.Ed25519.VerifyKey.from_base64(
                   "a4qcdbuJoRwy1ykc7Xhj1wTQroEXHdr04AY2Ds6SPb0",
                   "1"
                 ), System.system_time(:millisecond) + 604_800_000, false}
            }
          },
          server_name: "example.org"
        }
      )
    end
  end
end
