# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.WellKnownTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Server do
    defstruct []

    defimpl Polyjuice.Server.Protocols.Discovery do
      def address(_) do
        "https://example.org"
      end

      def identity_server(_) do
        nil
      end

      def extra_fields(_) do
        %{
          "foo" => "bar"
        }
      end

      def delegated_hostname(_) do
        URI.parse("https://example.org")
      end
    end
  end

  test "client .well-known file" do
    conn = conn(:get, "/.well-known/matrix/client")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Server{}
      )

    assert Enum.member?(conn.resp_headers, {"access-control-allow-origin", "*"})

    assert Jason.decode!(conn.resp_body) == %{
             "foo" => "bar",
             "m.homeserver" => %{
               "base_url" => "https://example.org"
             }
           }
  end

  test "server .well-known file" do
    conn = conn(:get, "/.well-known/matrix/server")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Server{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "m.server" => "example.org:443"
           }
  end
end
