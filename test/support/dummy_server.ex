# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule DummyFederationServer do
  defstruct [:keys, :old_keys, :remote_keys, :server_name]

  defimpl Polyjuice.Server.Protocols.BaseFederation do
    def get_keys(%{keys: keys}) do
      keys
    end

    def get_old_keys(%{old_keys: old_keys}) do
      old_keys
    end

    def get_cached_remote_keys(%{remote_keys: remote_keys}, origin, nil) do
      Map.get(remote_keys, origin, %{})
    end

    def get_cached_remote_keys(%{remote_keys: remote_keys}, origin, key_ids)
        when is_list(key_ids) do
      Map.get(remote_keys, origin, %{}) |> Map.take(key_ids)
    end

    def cache_remote_keys(_, _, _) do
      :ok
    end

    def get_server_name(%{server_name: server_name}) do
      server_name
    end
  end
end

defmodule DummyClientServer do
  defstruct [:access_tokens, :appservice_users, :server_name]

  defimpl Polyjuice.Server.Protocols.BaseClientServer do
    def user_from_access_token(%{access_tokens: access_tokens}, access_token) do
      Map.get(access_tokens, access_token)
    end

    def get_server_name(%{server_name: server_name}) do
      server_name
    end

    def user_in_appservice_namespace?(%{appservice_users: nil}, _, _) do
      false
    end

    def user_in_appservice_namespace?(
          %{appservice_users: appservice_users},
          appservice_user_id,
          user_id
        ) do
      Map.get(appservice_users, appservice_user_id) |> Enum.member?(user_id)
    end
  end
end
